class FormModel extends BaseModel
{
    constructor() {

        super();

        this.components = new ComponentsModel();

        this.rootName = "formRoot";

    }

    init() {

        this.components.init();

    }

    addRootComponent(name) {

        this.components.addRootComponent(name);
    }


}