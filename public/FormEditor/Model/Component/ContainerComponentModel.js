class ContainerComponentModel extends BaseComponentModel
{
    static propertiesNames = null;
    
    constructor(componentsModel) {

        super(componentsModel);
        
        this.children = [];
    }

}

ContainerComponentModel.propertiesNames = new Set([
    "padding-left",
    ...(BaseComponentModel.propertiesNames)
]);