var stc = {};
class FormRootComponent extends React.Component {

    constructor(props) {
        
        super();

        this.state = props.state;

        props.form.updateState = this.update.bind(this);
    }

    update(newState) {

        this.setState(newState);
    }

    createChild(child, index) {

        return <AppComponent

            key={child.id}

            state={child}
        >

        </AppComponent>;
    }

    render() {

        return (
            <this.state.htmlTag
                id={this.state.id}
                {...this.state.attributes}

                draggable={false}
                
            >

                {this.state.content}

                {this.state.children.map(this.createChild.bind(this))}

            </this.state.htmlTag>

        );

    }
}