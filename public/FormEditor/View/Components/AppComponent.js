
class AppComponent extends React.Component {

    constructor(props) {
        
        super();

        this.state = props.state;
    }

    createChild(child, index) {

        return <AppComponent

            key={child.id}

            state={child}
        >

        </AppComponent>;
    }

    handleChange(event) {
        //this.setState({value: event.target.value});
      }

    render() {

        if(typeof this.props.state.children === "undefined")
            return (
                <this.state.htmlTag
                    id = {this.props.state.id}
                    {...this.props.state.attributes}
                    {...this.props.state.emptyAttributes}
                    onChange={this.handleChange}
                    draggable={false}
                >

                    {this.props.state.textContent}

                </this.state.htmlTag>
            );
        else
            return (
                <this.state.htmlTag
                    id = {this.props.state.id}
                    {...this.props.state.attributes}
                    {...this.props.state.emptyAttributes}
                    draggable={false}
                >

                    {this.props.state.children.map(this.createChild.bind(this))}

                </this.state.htmlTag>

            );

    }
}


