script =
{
    "store":{
        "records":[],
        "record": {
            "value": "",
            "enabled": true,
            "id": Date.now()
        },
        // "emptyVal": "",
        "pathVal": "value",
        // "pathVal": "Duplicate key",
        "disabled": false,
        "enabledPath": "enabled",
        "NewTestKey": "NewTestValue"
    },
    "nodes": [
        {
            "type": "Container",
            "name": "Do Something",
            "id": 200,
            "pid": 0,
            "cid": 1,
            "x": 1220,
            "y": 700,
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "valueIn1",
                    "valueIn2"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "valueOut1",
                    "valueOut2"
                ]
            }
        },
       {
            "type": "InitialContainer",
            "pid": 200,
            "id": 3000,
            "x": 965,
            "y": 974,
            // "Defaults": {
            //     "Inputs": [
            //         {
            //             "index" : 0,
            //             "value" : ""
            //         }
            //     ]
            // },
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "valueIn1",
                    "valueIn2"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "valueOut1",
                    "valueOut2"
                ]
            }
        },
        {
            "name": "appendElement",
            "type": "JavaScriptBlock",
            "id": 3002,
            "pid": 200,
            "x": 1350,
            "y": 974,
            "params": [
                {
                    "ObjectName": "jspart",
                    "input0": "appendElement"
                }
            ],
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "array",
                    "element"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "resultArray"
                ]
            }
        },

        {
            "type": "TerminalContainer",
            "pid": 200,
            "id": 3001,
            "x": 1800,
            "y": 974,
            // "Defaults": {
            //     "Inputs": [
            //         {
            //             "index" : 0,
            //             "value" : ""
            //         }
            //     ]
            // },
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "valueIn1",
                    "valueIn2"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "valueOut1",
                    "valueOut2"
                ]
            }
        },
        {
            "type": "Handler",
            "pid": 0,
            "id": 6,
            "x": 59,
            "y": 635,
            "params": [
                {
                    "eventName": "addRecord"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": []
            }
        },
        {
            "type": "HandlerWithParams",
            "pid": 0,
            "id": 11,
            "x": 66,
            "y": 434,
            "params": [
                {
                    "eventName": "updateRecord",
                    "value": ""
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "value"
                ]
            }
        },
        {
            "type": "ToStoreElement",
            "pid": 0,
            "id": 12,
            "x": 312,
            "y": 326,
            "params": [
                {
                    "ElementName": "record"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": [
                    "ToStore"
                ]
            },
            "Outputs": {
                "flow": [],
                "data": []
            }
        },
        {
            "name": "appendElement",
            "type": "JavaScriptBlock",
            "pid": 0,
            "id": 15,
            "x": 1097,
            "y": 356,
            "params": [
                {
                    "ObjectName": "jspart",
                    "input0": "appendElement"
                }
            ],
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "array",
                    "element"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "resultArray"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 16,
            "x": 795,
            "y": 786,
            "params": [
                {
                    "ElementName": "records"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 17,
            "x": 801,
            "y": 903,
            "params": [
                {
                    "ElementName": "record"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "ToStoreElement",
            "pid": 0,
            "id": 18,
            "x": 1331,
            "y": 539,
            "params": [
                {
                    "ElementName": "records"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": [
                    "ToStore"
                ]
            },
            "Outputs": {
                "flow": [],
                "data": []
            }
        },
        {
            "name": "log",
            "type": "JavaScriptBlock",
            "pid": 0,
            "id": 19,
            "x": 646,
            "y": 1013,
            "params": [
                {
                    "ObjectName": "jspart",
                    "input0": "log"
                }
            ],
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "value"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": []
            }
        },
        // {
        //     "type": "FromStoreElement",
        //     "id": 24,
        //     "x": 1361,
        //     "y": 752,
        //     "params": [
        //         {
        //             "ElementName": "emptyVal"
        //         }
        //     ],
        //     "Inputs": {
        //         "flow": [],
        //         "data": []
        //     },
        //     "Outputs": {
        //         "flow": [],
        //         "data": [
        //             "FromStore"
        //         ]
        //     }
        // },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 25,
            "x": 1361,
            "y": 843,
            "params": [
                {
                    "ElementName": "record"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "SetValue",
            "pid": 0,
            "id": 26,
            "x": 1665,
            "y": 674,
            "Defaults": {
                "Inputs": [
                    {
                        "index" : 0,
                        "value" : ""
                    }
                ]
            },
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "value",
                    "json",
                    "path"
                ]
            },
            "Outputs": {
                "flow": [
                    "out",
                    "error"
                ],
                "data": [
                    "json"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 27,
            "x": 1359,
            "y": 938,
            "params": [
                {
                    "ElementName": "pathVal"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "ToStoreElement",
            "pid": 0,
            "id": 28,
            "x": 1927,
            "y": 539,
            "params": [
                {
                    "ElementName": "record"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": [
                    "ToStore"
                ]
            },
            "Outputs": {
                "flow": [],
                "data": []
            }
        },
        {
            "type": "If",
            "pid": 0,
            "id": 29,
            "x": 738,
            "y": 653,
            "Defaults": {
                "Inputs": [
                    {
                        "index" : 1,
                        "value" : ""
                    }
                ]
            },
            "params": [
                {
                    "condition": "=="
                }
            ],
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "a",
                    "b"
                ]
            },
            "Outputs": {
                "flow": [
                    "then",
                    "else"
                ],
                "data": []
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 30,
            "x": 449,
            "y": 785,
            "params": [
                {
                    "ElementName": "emptyVal"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 31,
            "x": 146,
            "y": 735,
            "params": [
                {
                    "ElementName": "record"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "GetValue",
            "pid": 0,
            "id": 32,
            "x": 443,
            "y": 653,
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "json",
                    "path"
                ]
            },
            "Outputs": {
                "flow": [
                    "out",
                    "error"
                ],
                "data": [
                    "value"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 33,
            "x": 146,
            "y": 830,
            "params": [
                {
                    "ElementName": "pathVal"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "HandlerWithParams",
            "pid": 0,
            "id": 34,
            "x": 17,
            "y": 147,
            "params": [
                {
                    "eventName": "strikeOut",
                    "value": ""
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "value"
                ]
            }
        },
        {
            "name": "toString",
            "type": "JavaScriptBlock",
            "pid": 0,
            "id": 36,
            "x": 344,
            "y": 171,
            "params": [
                {
                    "ObjectName": "jspart",
                    "input0": "toString"
                }
            ],
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "value"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "result"
                ]
            }
        },
        {
            "type": "GetValue",
            "pid": 0,
            "id": 37,
            "x": 668,
            "y": 111,
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "json",
                    "path"
                ]
            },
            "Outputs": {
                "flow": [
                    "out",
                    "error"
                ],
                "data": [
                    "value"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 38,
            "x": 341,
            "y": 8,
            "params": [
                {
                    "ElementName": "records"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "SetValue",
            "pid": 0,
            "id": 39,
            "x": 1544,
            "y": 62,
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "value",
                    "json",
                    "path"
                ]
            },
            "Outputs": {
                "flow": [
                    "out",
                    "error"
                ],
                "data": [
                    "json"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 41,
            "x": 1248,
            "y": 254,
            "params": [
                {
                    "ElementName": "enabledPath"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 42,
            "x": 1617,
            "y": 243,
            "params": [
                {
                    "ElementName": "records"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "type": "SetValue",
            "pid": 0,
            "id": 43,
            "x": 1859,
            "y": 83,
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "value",
                    "json",
                    "path"
                ]
            },
            "Outputs": {
                "flow": [
                    "out",
                    "error"
                ],
                "data": [
                    "json"
                ]
            }
        },
        {
            "type": "ToStoreElement",
            "pid": 0,
            "id": 44,
            "x": 2106,
            "y": 14,
            "params": [
                {
                    "ElementName": "records"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": [
                    "ToStore"
                ]
            },
            "Outputs": {
                "flow": [],
                "data": []
            }
        },
        {
            "name": "inversion",
            "type": "JavaScriptBlock",
            "pid": 0,
            "id": 45,
            "x": 1254,
            "y": 32,
            "params": [
                {
                    "ObjectName": "jspart",
                    "input0": "inversion"
                }
            ],
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "value"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": [
                    "result"
                ]
            }
        },
        {
            "type": "GetValue",
            "pid": 0,
            "id": 46,
            "x": 974,
            "y": 5,
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "json",
                    "path"
                ]
            },
            "Outputs": {
                "flow": [
                    "out",
                    "error"
                ],
                "data": [
                    "value"
                ]
            }
        },
        {
            "type": "FromStoreElement",
            "pid": 0,
            "id": 47,
            "x": 671,
            "y": 5,
            "params": [
                {
                    "ElementName": "enabledPath"
                }
            ],
            "Inputs": {
                "flow": [],
                "data": []
            },
            "Outputs": {
                "flow": [],
                "data": [
                    "FromStore"
                ]
            }
        },
        {
            "name": "log",
            "type": "JavaScriptBlock",
            "pid": 0,
            "id": 48,
            "x": 2058,
            "y": 667,
            "params": [
                {
                    "ObjectName": "jspart",
                    "input0": "log"
                }
            ],
            "Inputs": {
                "flow": [
                    "in"
                ],
                "data": [
                    "value"
                ]
            },
            "Outputs": {
                "flow": [
                    "out"
                ],
                "data": []
            }
        }
    ],
    "connections": [
        {
            "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 200,
                "index": 0
            },
            "dest": {
                "nodeID": 18,
                "index": 0
            }
        },
        {
            "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 16,
                "index": 0
            },
            "dest": {
                "nodeID": 200,
                "index": 0
            }
        },
        {
            "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 17,
                "index": 0
            },
            "dest": {
                "nodeID": 200,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 3000,
                "index": 0
            },
            "dest": {
                "nodeID": 3002,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 3002,
                "index": 0
            },
            "dest": {
                "nodeID": 3001,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 3002,
                "index": 0
            },
            "dest": {
                "nodeID": 3001,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 3000,
                "index": 0
            },
            "dest": {
                "nodeID": 3002,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 3000,
                "index": 1
            },
            "dest": {
                "nodeID": 3002,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 11,
                "index": 0
            },
            "dest": {
                "nodeID": 12,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 16,
                "index": 0
            },
            "dest": {
                "nodeID": 15,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 17,
                "index": 0
            },
            "dest": {
                "nodeID": 15,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 15,
                "index": 0
            },
            "dest": {
                "nodeID": 18,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 200,
                "index": 0
            },
            "dest": {
                "nodeID": 26,
                "index": 0
            }
        },
        // {
        //     "type": "data",
        //     "source": {
        //         "nodeID": 24,
        //         "index": 0
        //     },
        //     "dest": {
        //         "nodeID": 26,
        //         "index": 0
        //     }
        // },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 25,
                "index": 0
            },
            "dest": {
                "nodeID": 26,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 27,
                "index": 0
            },
            "dest": {
                "nodeID": 26,
                "index": 2
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 29,
                "index": 1
            },
            "dest": {
                "nodeID": 200,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 31,
                "index": 0
            },
            "dest": {
                "nodeID": 32,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 33,
                "index": 0
            },
            "dest": {
                "nodeID": 32,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 6,
                "index": 0
            },
            "dest": {
                "nodeID": 32,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 32,
                "index": 0
            },
            "dest": {
                "nodeID": 29,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 32,
                "index": 0
            },
            "dest": {
                "nodeID": 29,
                "index": 0
            }
        },
        // {
        //     "type": "data",
        //     "source": {
        //         "nodeID": 30,
        //         "index": 0
        //     },
        //     "dest": {
        //         "nodeID": 29,
        //         "index": 1
        //     }
        // },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 34,
                "index": 0
            },
            "dest": {
                "nodeID": 36,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 34,
                "index": 0
            },
            "dest": {
                "nodeID": 36,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 36,
                "index": 0
            },
            "dest": {
                "nodeID": 37,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 36,
                "index": 0
            },
            "dest": {
                "nodeID": 37,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 38,
                "index": 0
            },
            "dest": {
                "nodeID": 37,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 37,
                "index": 0
            },
            "dest": {
                "nodeID": 39,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 41,
                "index": 0
            },
            "dest": {
                "nodeID": 39,
                "index": 2
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 36,
                "index": 0
            },
            "dest": {
                "nodeID": 43,
                "index": 2
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 42,
                "index": 0
            },
            "dest": {
                "nodeID": 43,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 39,
                "index": 0
            },
            "dest": {
                "nodeID": 43,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 39,
                "index": 0
            },
            "dest": {
                "nodeID": 43,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 43,
                "index": 0
            },
            "dest": {
                "nodeID": 44,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 37,
                "index": 0
            },
            "dest": {
                "nodeID": 46,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 47,
                "index": 0
            },
            "dest": {
                "nodeID": 46,
                "index": 1
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 37,
                "index": 0
            },
            "dest": {
                "nodeID": 46,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 46,
                "index": 0
            },
            "dest": {
                "nodeID": 45,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 45,
                "index": 0
            },
            "dest": {
                "nodeID": 39,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 45,
                "index": 0
            },
            "dest": {
                "nodeID": 39,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "flow",
            "source": {
                "nodeID": 46,
                "index": 0
            },
            "dest": {
                "nodeID": 45,
                "index": 0
            }
        },
        {
             "pid": 0,
            "type": "data",
            "source": {
                "nodeID": 26,
                "index": 0
            },
            "dest": {
                "nodeID": 28,
                "index": 0
            }
        }
    ]
}