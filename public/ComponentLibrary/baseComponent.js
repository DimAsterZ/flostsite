'use strict';

let addActionToList = function (name) {

    let handler = function(event) {
        
        Module.Store.dispatch({
            'eventName': name,
            'value': event.target.value
        });

    }

    ListActions[name] = handler;

    return handler;
}

let tagToComponent = {

}



let globStoreChange;
let globCompState;

class ButtonEl extends React.Component {

    constructor(props) {
        super();
        if (props.index === 2) {
            this.zhopa = props.zhopa;
        }
        else {
            this.zhopa = "kaka";
        }
        this.content = props.content;
        this.state = props.initState;

    }

    render() {
        
            return (
                <button  >

                    {this.content + this.zhopa}
                    
                </button>

            );
    }
}

class TodoList extends React.Component {

    constructor(props) {
        super();
        
        this.initState = props.initState;
        console.log(this.state);
    }

    createChild2(child, index) {

        let attributes = {
            style : {
                display:"block",
                width : "40px",
                height: "40px",
                color : this.initState.colors[index%3]
            }
        }
        
        return <button {...attributes} >child</button>;

        // return <ButtonEl content = {child} index = {index} zhopa = "addedText" atr = {attributes} ></ButtonEl>;
    }

    render() {
        
            return (
                <div>

                    {this.initState.children.map(this.createChild2.bind(this))}
                    
                </div>

            );
    }
}


class AppElement extends React.Component {

    constructor(props) {
        super();
        
        this.initState = props.initState;
        this.state = props.initState;

        //this.setState(props.initState);
        this.state.childrenStoreChange = [];

        this.initActions(this.initState.attributes);

//         if(this.state.name == 'root'){
//             globCompState = this.state;
// f

//             globStoreChange = this.storeChanged.bind(this);
//         }

        // if(this.state.name != 'root')
        //     props.onStoreChangeHandlers.push(this.storeChanged.bind(this));
    }

    storeChanged(newState) {
        //console.log("storeChanged begin", this.state.name);
        const oldState = Object.assign(this.state);
        
        console.log("new rootState", newState);

        if(this.state.name == 'root') {
            const sCopy = JSON.parse(JSON.stringify(Module.Store.getAll().rootState));
            
            //console.log("storeChanged", sCopy);
            this.setState(sCopy);
            
            globCompState = this.state;

            
        }
        else {
            //console.log("newState", newState);
            this.setState(newState);
        }
        
        // const children = this.state.children;
        // const childrenStoreChange = oldState.childrenStoreChange;

        // for(let i = 0; i < children.length; ++i) {
            
        //     childrenStoreChange[i](children[i]);
        // }
        
    }

    initActions() {
        
        for(let action of this.initState.actions) {
            
            const handler = addActionToList(action.handler);

            this.initState.attributes[action.event] = handler.bind(this);
        }
    }

    componentDidMount() {
        
        //this.setState(this.initState);
        //if(this.state.name == 'root')
            //Event.bind('change', this.storeChanged.bind(this));
    }

    

    createChild(child, index) {

        const T = tagToComponent[child.type];

        return <T

            key = {index}

            initState = {child}
            
            // onStoreChangeHandlers = {this.state.childrenStoreChange}

        > </T>;



    }

    render() {
        
        //console.log("STTTTTTTTTTTTTTTA", this.state);
        //console.log("STTTTTTTTTTTTTTTA2", this.state.children.length);
        if(typeof this.state.content === 'undefined')
            this.state.content="";

        if(this.state.type != "input")
            return (
                <this.state.type {...this.state.attributes} >

                    {this.state.content}

                    {this.state.children.map(this.createChild.bind(this))}
                    
                </this.state.type>

            );
        else
            return (
                <this.state.type {...this.state.attributes} >
                    
                </this.state.type>
            );
    }
}

tagToComponent = {
    'div' : AppElement,
    'span' : AppElement,
    'button' : AppElement,
    'svg' : AppElement,
    'path' : AppElement,
    'input' : AppElement,
    'li' : AppElement,
    'ol' : AppElement
}