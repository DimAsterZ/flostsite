

class AppComponent extends React.Component {

    constructor(props) {
        super();

        this.initState = props.initState;
        this.state = props.initState;
    }

    createChild(child, index) {

        return <AppComponent

            key={index}

            initState={child}
        >
        </AppComponent>;

    }

    render() {


        if(this.state.htmlTag != "input")
            return (
                <this.state.htmlTag {...this.state.attributes} >

                    {this.state.content}

                    {this.state.children.map(this.createChild.bind(this))}
                    
                </this.state.htmlTag>

            );
        else
            return (
                <this.state.htmlTag {...this.state.attributes} >
                    
                </this.state.htmlTag>
            );

    }
}


