/* eslint-disable max-lines-per-function */
/* eslint-disable max-lines */
/* eslint-disable max-len */
/* eslint-disable no-continue */
/* global GlobalNodes BasicConnection GlobalConnections MainNode scrollCorrect*/
/* exported BasicNode*/
'use strict';

// OutFlows: [{…}]
// id: 101
// params: [{…}]
// type: "StoreElement"
// x: 0
// y: 0
class Slot {

	constructor(index, type, name, value, html, side) {
		this.index = index;			// Индекс блока
		this.type = type;			// Тип блока
		this.name = name;			// Имя блока
		this.value = value;			// Значение блока
		this.html = html;			// 
		this.side = side;			// 
		this.lines = [];			// 
		this.store = null;			// 
		this.defaults = null;		// 
	}

	appendStoreToSlot(name) {
		// debugger;
		// this -> slot

		// CREATE STORE BLOCK
		const block = {
			id: ++APP.index,
			x: -1,
			y: -1,
			params: [{
				ElementName: name
			}],
			Inputs: {
				flow: [],
				data: []
			},
			Outputs: {
				flow: [],
				data: []
			}
		};

		let s_n = -1;
		let s_i = -1;
		let d_n = -1;
		let d_i = -1;

		if (this.side === "IN") {
			// FromStoreElement
			//{"type":"FromStoreElement","id":-1,"x":-1,"y":-1,"params":[{"ElementName":"NAME"}],"Inputs":{"flow":[],"data":[]},"Outputs":{"flow":[],"data":["FromStore"]}}
			block.type = "FromStoreElement";
			block.Outputs.data.push("FromStore");


			s_n = block.id;
			s_i = 0;
			d_n = this.nodeID;
			d_i = this.index;
		} else {
			// ToStoreElement
			//"{"type":"ToStoreElement","id":-1,"x":-1,"y":-1,"params":[{"ElementName":"NAME"}],"Inputs":{"flow":[],"data":["ToStore"]},"Outputs":{"flow":[],"data":[]}}"
			block.type = "ToStoreElement";
			block.Inputs.data.push("ToStore");

			s_n = this.nodeID;
			s_i = this.index;
			d_n = block.id;
			d_i = 0;

		}
		APP.Logic.appendStoreBlock(block);

		// CONNECT STORE
		// "{"type":"data","source":{"nodeID":11,"index":0},"dest":{"nodeID":12,"index":0}}"
		const connection = {
			type: "data",
			source: {
				nodeID: s_n,
				index: s_i
			},
			"dest": {
				nodeID: d_n,
				index: d_i
			}
		};

		APP.Logic.nodes[this.nodeID].appendStore(connection);
		APP.Logic.storesConnections.push(connection);

		// debugger;
	}
	
}

class BasicNode {

	constructor(object) {
		this.id = object.id;
		this.type = object.type;
		
		this.pid = object.pid;
		this.cid = object.cid;
		
		this.x = object.x;
		this.y = object.y;
		
		if (("params" in object) && object.params.length > 0) {
			this.defParam = object.params;
		}

		this.slots = [];

		this.flowIn = {};
		this.flowOut = {};
		this.dataIn = {};
		this.dataOut = {};

		this.line = null;
		this.slot = null;
		this.lines = [];

		this.param = "";
		this.storeVal = "";
		this.name = "";
		// let valEl = false;
		if (("params" in object) && object.params.length > 0) {
			if ("eventName" in object.params[0]) {
				this.param = object.params[0].eventName;
			}
			if ("ElementName" in object.params[0]) {
				// valEl = true;
				this.storeVal = object.params[0].ElementName;
			}
		}
		if ("name" in object) {
			this.name = object.name;
		}
		
		this.createHtml();

		this.pressed = false;
		this.pressX = null;
		this.pressY = null;

		// Parse Slots
		this.parseSlots(object, "Inputs");
		this.parseSlots(object, "Outputs");
		
		
		// this.i_Defaults = [];
		// this.o_Defaults = [];
		
		// Defaults
		this.parseDefaults(object);
		
	}
	
	
	createHtml() {
		
		// BASIC CONTAINER
		this.htmlElement = document.createElement("div");
		this.htmlElement.id = "N_" + this.id;
		this.htmlElement.classList.add("node");
		this.htmlElement.style.top = this.y + "px";
		this.htmlElement.style.left = this.x + "px";
		
		// TYPE AND ID
		this.htmlTypeContainer = document.createElement("div");
		this.htmlTypeContainer.classList.add("node-type");
		this.htmlElement.appendChild(this.htmlTypeContainer);
		
		// MORE INFO CONTAINER
		this.htmlMInfoContainer = document.createElement("div");
		this.htmlMInfoContainer.classList.add("node-info");
		this.htmlElement.appendChild(this.htmlMInfoContainer);
		
		// CONNECTIONs container
		this.container = document.createElement("div");
		this.container.classList.add("containers");
		this.htmlElement.appendChild(this.container);
		
		this.containerLEFT = document.createElement("div");
		this.containerLEFT.classList.add("connection_container");
		this.container.appendChild(this.containerLEFT);
		
		this.containerRIGHT = document.createElement("div");
		this.containerRIGHT.classList.add("connection_container");
		this.container.appendChild(this.containerRIGHT);
		
		this.inFlow = document.createElement("div");
		this.inFlow.classList.add("F");
		this.containerLEFT.appendChild(this.inFlow);
		
		this.outFlow = document.createElement("div");
		this.outFlow.classList.add("F");
		this.containerRIGHT.appendChild(this.outFlow);
		
		this.inData = document.createElement("div");
		this.inData.classList.add("D");
		this.containerLEFT.appendChild(this.inData);
		
		this.outData = document.createElement("div");
		this.outData.classList.add("D");
		this.containerRIGHT.appendChild(this.outData);
		
		
		// BOTTOM INDENT 1
		this.bottomDiv = document.createElement("div");
		this.bottomDiv.classList.add("node-b-indent1");
		this.htmlElement.appendChild(this.bottomDiv);
		
		// BOTTOM INDENT 2
		this.bottomDiv1 = document.createElement("div");
		this.bottomDiv1.classList.add("node-b-indent2");
		this.htmlElement.appendChild(this.bottomDiv1);
		
		
		// let ccc = this.htmlMInfoContainer;
		let ccc = this.htmlTypeContainer;
		if (this.name !== "") {
			// this.nameElement = document.createElement("div");
			// this.nameElement.style.color = "green";
			// this.nameElement.style.fontWeight = "bold";
			// this.nameElement.innerText = this.name;
			// ccc.appendChild(this.nameElement);
			
			
			// ccc.style.color = "green";//redesign
			// ccc.style.fontWeight = "bold";//redesign
			ccc.innerText = this.name;
			
		}
		
		if (this.storeVal !== "") {
			// this.valElement = document.createElement("div");
			// this.valElement.style.color = "blue";
			// this.valElement.style.fontWeight = "bold";
			// this.valElement.innerText = this.storeVal;
			// ccc.appendChild(this.valElement);
			
			ccc.style.color = "blue";
			ccc.style.fontWeight = "bold";
			ccc.innerText = this.storeVal;
		}
		
		if (this.param !== "") {
			// this.eventName = document.createElement("div");
			// this.eventName.style.color = "red";
			// this.eventName.style.fontWeight = "bold";
			// this.eventName.innerText = this.param;
			// ccc.appendChild(this.eventName);
			
			//redesign
			//ccc.style.color = "red";
			//ccc.style.fontWeight = "bold";
			ccc.innerText = this.param;
		}
		
		// this.htmlTypeContainer.innerText = this.type + "\n" + this.id + "\n\n";
		// this.htmlTypeContainer.innerText = this.type;
		this.bottomDiv1.innerText = this.type;
		// this.bottomDiv1.style.paddingLeft = "15px";
		this.htmlMInfoContainer.style.height = "5px";
		this.bottomDiv.style.height = "5px";
		this.bottomDiv1.style.textAlign = "right";
		
	}

	parseSlots(object, slotsName) {
		if (slotsName in object) {
			if ("flow" in object[slotsName]) {
				for (const flowOut of object[slotsName].flow) {
					if (!flowOut) {
						continue;
					}
					if (slotsName === "Inputs") {
						this.addLeftConnection(flowOut, "flow");
					} else {
						this.addRightConnection(flowOut, "flow");
					}
				}
			}
			if ("data" in object[slotsName]) {
				for (const dataOut of object[slotsName].data) {
					if (!dataOut) {
						continue;
					}
					if (slotsName === "Inputs") {
						this.addLeftConnection(dataOut, "data");
					} else {
						this.addRightConnection(dataOut, "data");
					}
				}
			}
		}
	}
	
	parseDefaults(object) {
		
		// Check Defaults
		if (("Defaults" in object)) {
			
			// Check Def Inputs
			if ("Inputs" in object.Defaults) {
				const i_Defaults = object.Defaults.Inputs;
				
				for (const def of i_Defaults) {
					const slot = this.getSlotId("data", "IN", def.index);
					if (!slot) {
						continue;
					}
					
					// slot.defaults = c;
					// if (slot.side === "OUT") {
					// 	slot.html.style.borderRight = "5px solid green";
					// } else {
					// 	slot.html.style.borderLeft = "5px solid green";
					// }
					
					this.appendDefaults(slot, def);
					
				}
				
			}
			
			// Check Def Outputs
			if ("Outputs" in object.Defaults) {
				const o_Defaults = object.Defaults.Outputs;
				
				for (const def of o_Defaults) {
					const slot = this.getSlotId("data", "OUT", def.index);
					if (!slot) {
						continue;
					}
					this.appendDefaults(slot, def);
				}
				
			}
			
		}
	}

	addLeftConnection(name, type, line) {

		if (line) {
			this.lines.push(line);
		}

		if (type === "flow") {
			const c = this.flowIn[name];
			if (typeof c !== "undefined") {
				return c;
			}
		} else {
			const c = this.dataIn[name];
			if (typeof c !== "undefined") {
				return c;
			}
		}

		const connection = document.createElement("div");
		connection.classList.add("connection");
		if (type === "data")
			connection.ondblclick = this.dblClickConnectionSlot.bind(this);

		const point = document.createElement("div");
		point.classList.add("c_point");
		connection.appendChild(point);

		const text = document.createElement("div");
		text.classList.add("fillText");
		text.innerText = name;
		connection.appendChild(text);

		let index = -1;
		if (type === "flow") {
			index = Object.keys(this.flowIn).length;
			this.inFlow.appendChild(connection);
			this.flowIn[name] = connection;
		} else {
			index = Object.keys(this.dataIn).length;
			this.inData.appendChild(connection);
			this.dataIn[name] = connection;
			//point.style.border = "1px blue solid";//redesign
		}

		point.classList.add(type === 'flow' ? 'flow_point' : 'data_point');//redesign

		const s = new Slot(index, type, name, null, connection, "IN");
		s.nodeID = this.id;
		this.slots.push(s);
		return connection;
	}

	addRightConnection(name, type, line) {

		if (line) {
			this.lines.push(line);
		}

		if (type === "flow") {
			const c = this.flowOut[name];
			if (typeof c !== "undefined") {
				return c;
			}
		} else {
			const c = this.dataOut[name];
			if (typeof c !== "undefined") {
				return c;
			}
		}

		const connection = document.createElement("div");
		connection.classList.add("connection");
		connection.classList.add("right");
		if (type === "data")
			connection.ondblclick = this.dblClickConnectionSlot.bind(this);

		const text = document.createElement("div");
		text.classList.add("fillText");
		text.innerText = name;
		connection.appendChild(text);

		const point = document.createElement("div");
		point.classList.add("c_point");
		connection.appendChild(point);

		let index = -1;
		if (type === "flow") {
			index = Object.keys(this.flowOut).length;
			this.outFlow.appendChild(connection);
			this.flowOut[name] = connection;
		} else {
			index = Object.keys(this.dataOut).length;
			this.outData.appendChild(connection);
			this.dataOut[name] = connection;
			//point.style.border = "1px blue solid";//redesign
		}

		point.classList.add(type === 'flow' ? 'flow_point' : 'data_point');//redesign

		const s = new Slot(index, type, name, null, connection, "OUT");
		s.nodeID = this.id;
		this.slots.push(s);
		return connection;
	}
	
	dblClickConnectionSlot(event) {
		
		console.log("dbl_Press");
		
		const slot = this.getSlot(event.target);
		
		if (!slot) {
			console.error("Slot is null!");
			return;
		}
		
		if (!this.checkSlotAppended(slot)) {
			return;
		}
		
		if (slot.store) {
			console.error("Slot Store already exist!");
			return;
		}
		
		// APP.UI.getStoreList(this.appendStoreToSlot.bind(slot));
		APP.UI.getStoreList((slot));
		
	}

	checkSlotAppended(slot) {
		
		if (!slot) {
			return false;
		}
		
		if ((slot.type === "data" && slot.side === "IN") || (slot.type === "flow" && slot.side === "OUT")) {
			if (slot.lines.length || slot.store) {
				console.error("SLOT ALREADY EXIST");
				return false;
			}
		}
		
		return true;
	}

	mouseDown(event) {
		
		this.pressX = event.offsetX ;
		this.pressY = event.offsetY ;
		
		if (event.target.classList.contains("node-type")) {
			this.pressed = true;
		} else if (event.target.classList.contains("connection")) {
			
			this.slot = this.getSlot(event.target);
			if (!this.slot) {
				return;
			}
			
			//CHECK ON ONE
			
			// INPUTS vs EVENTS
			if (!this.checkSlotAppended(this.slot)) {
				return;
			}
			
			let indexSlot = this.slot.index;
			// const childs = event.target.parentElement.childNodes;
			// for (let i = 0; i < childs.length; ++i) {
				// if (childs[i] === event.target) {
					// indexSlot = i;
				// }
			// }
			
			const o = {
				"type": this.slot.type,
				"dest": {
					"nodeID": this.id,
					"index": indexSlot
				},
				"source": {
					"nodeID": this.id,
					"index": indexSlot
				}
			};

			this.line = new BasicConnection(o);
			document.getElementById("container").appendChild(this.line.htmlElement);

			const px1 = this.htmlElement.offsetLeft + this.slot.html.children[0].offsetLeft;
			const py1 = this.htmlElement.offsetTop + this.slot.html.children[0].offsetTop;

			this.line.setPos(px1, py1, px1, py1);

		}

	}

	mouseMove(event) {
		if (this.pressed === true) {
			
			this.htmlElement.style.left = APP.UI.getSceneOffset(event, 0) - this.pressX + "px";
			this.htmlElement.style.top = APP.UI.getSceneOffset(event, 1) - this.pressY + "px";
				
			if (this.htmlElement.offsetLeft < 300) {
				this.htmlElement.style.left = "300px";
			}
			
			for (const l of this.lines) {
				l.updatePositions();
			}
			
			
			
		} else if (this.line && this.slot) {
			if (this.slot.side === "IN") {
				const px1 = this.htmlElement.offsetLeft + this.slot.html.children[0].offsetLeft;
				const py1 = this.htmlElement.offsetTop + this.slot.html.children[0].offsetTop;
				this.line.setPos(px1, py1,
					 APP.UI.getSceneOffset(event, 0),
					 APP.UI.getSceneOffset(event, 1) + 10);
			} else {
				const px1 = this.htmlElement.offsetLeft + this.slot.html.children[1].offsetLeft;
				const py1 = this.htmlElement.offsetTop + this.slot.html.children[1].offsetTop;
				this.line.setPos(
					 APP.UI.getSceneOffset(event, 0),
					 APP.UI.getSceneOffset(event, 1) + 10, px1, py1);
			}
		}
		
	}

	mouseUp(event) {
		this.pressed = false;
		if (this.line) {
			let nO = null;
			const path = event.path || (event.composedPath && event.composedPath());
			for (const n of path) {
				if (!n) {
					continue;
				}
				if (!n.id) {
					continue;
				}
				if (n.classList.contains("node")) {
					
					let id = null;
					if (n.id.indexOf("N_") !== -1) {
						id = n.id.substr(2);
					}
					
					nO = APP.Logic.nodes[id];
					break;
				}
			}
			if (nO) {
				
				if (nO.viewArea !== this.viewArea) {
					this.line.destroy();
					delete this.line;
					this.line = null;
					return;
				}
				
				const slot = nO.getSlot(event.target);
				if (slot && (slot.type === this.slot.type) && (slot.side !== this.slot.side)) {
					
					const o = {
						"type": this.slot.type
					};
					const c1 = {
						"nodeID": this.id,
						"index": this.slot.index
					};
					const c2 = {
						"nodeID": nO.id,
						"index": slot.index
					};
					if (this.slot.side === "IN") {
						o.dest = c1;
						o.source = c2;
					} else {
						o.dest = c2;
						o.source = c1;
					}
					const l = new BasicConnection(o);
					APP.Logic.connections[l.id] = l;
					l.staticCreate();
					l.updatePositions();
					document.getElementById("container").appendChild(l.htmlElement);
				}
			}
			this.line.destroy();
			delete this.line;
			this.line = null;
		}
	}

	focused(bool) {
		if (bool) {
			this.htmlElement.classList.add("focused");
		} else {
			this.htmlElement.classList.remove("focused");
		}
	}

	destroy() {
		
		// for (const c of this.lines) {
		// 	if (!c) {
		// 		continue;
		// 	}
		// 	APP.Logic.removeObject(c)
		// 	// c.destroy();
		// }
		
		for (let i = 0; i < this.lines.length; ++i) {
			const c = this.lines[i]
			if (!c) {
				continue;
			}
			APP.Logic.removeObject(c)
			i--;
			// c.destroy();
		}
		
		
		this.htmlElement.remove();
		// delete GlobalNodes[this.id];
	}
	
	appendConnection(connection) {
		
		if (!connection) {
			return;
		}
		
		let slot = null;
		if (this.id === connection.nodeID1) {
			slot = this.getSlotId(connection.type, "IN", connection.index1);
		} else if (this.id === connection.nodeID2) {
			slot = this.getSlotId(connection.type, "OUT", connection.index2);
		}
		
		if (slot === null) {
			APP.Logic.removeObject(connection);
			return false;
		}
		
		if (!this.checkSlotAppended(slot)) {
			APP.Logic.removeObject(connection);
			return false;
		}
		
		// check on 
		
		slot.lines.push(connection);
		this.lines.push(connection);
		
		return true;
		
	}

	removeLine(line) {
		
		if (!line ) {
			return;
		}
		
		
		let slot = null;
		if (line.nodeID1 === this.id) {
			slot = this.getSlotId(line.type, "IN", line.index1);
		} else {
			slot = this.getSlotId(line.type, "OUT", line.index2);
		}
		
		if (slot) {
			const index = slot.lines.indexOf(line);
			if (index !== -1)
				slot.lines.splice(index, 1);
		}
		
		for (let i = 0; i < this.lines.length; ++i) {
			const l = this.lines[i];
			if (!l) {
				continue;
			}
			if (l === line) {
				this.lines.splice(i, 1);
				--i;
			}
		}
		
	}
	
	appendStore(data) {
		
		if (!data) {
			return false;
		}
		
		let slot = null;
		let storeBlock = null;
		if (this.id === data.dest.nodeID) {
			slot = this.getSlotId(data.type, "IN", data.dest.index);
			storeBlock = APP.Logic.storesBlocks[data.source.nodeID];
		} else if (this.id === data.source.nodeID) {
			slot = this.getSlotId(data.type, "OUT", data.source.index);
			storeBlock = APP.Logic.storesBlocks[data.dest.nodeID];
		}

		if (!slot || !storeBlock) {
			// APP.Logic.removeObject(connection);
			return false;
		}
		
		if (!this.checkSlotAppended(slot)) {
			return false;
		}
		
		slot.store = (data);
		if (slot.side === "OUT") {
			
			slot.html.style.borderRight = "5px solid var(--storeBlockValueTriggerColor)";
			//slot.html.style.borderRight = "5px solid red";
		} else {
			
			slot.html.style.borderLeft = "5px solid var(--storeBlockValueTriggerColor)";
			// slot.html.style.borderLeft = "5px solid red";
		}
		
		// STORE NAME
		const div = document.createElement("div");
		div.className = "StoreBlock";
		div.innerText = storeBlock.params[0].ElementName;
		slot.html.appendChild(div);
		
		// REMOVE 
		const removeDiv = document.createElement("div");
		removeDiv.innerText = "x";
		div.appendChild(removeDiv);
		
		removeDiv.onclick = function (e) {
			
			const storeConnection = this.store;
			
			this.html.style.borderLeft = "none";
			this.html.style.borderRight = "none";
			this.html.children[2].remove();
			
			APP.Logic.removeStoreConnection(this.store);
			this.store = null;
			
		}.bind(slot)
		
		
		// FIXED
		const fixedDiv = document.createElement("div");
		fixedDiv.className = "Fixed";
				fixedDiv.innerText = "!";
				div.appendChild(fixedDiv);

				fixedDiv.onclick = function (e) {

					const div = e.target.parentElement;
					
					if (div.classList.contains("F")) {
						div.classList.remove("F");
						// e.target.style.background = "";
					} else {
						// e.target.style.font = "#459c4b";
						div.classList.add("F");
					}

				}
		
		return true;
		
	}
	
	appendDefaults(slot, def) {
		
		if (!this.checkSlotAppended(slot)) {
			return false;
		}
		
		slot.defaults = (def);
		if (slot.side === "OUT") {
			// slot.html.style.borderRight = "5px solid green";
			slot.html.style.borderRight = "5px solid var(--defaultBlockValueTriggerColor)";
		} else {
			// slot.html.style.borderLeft = "5px solid green";
			slot.html.style.borderLeft = "5px solid var(--defaultBlockValueTriggerColor)";
		}
		
		// STORE NAME
		const div = document.createElement("div");
		div.className = "StoreBlock";
		div.innerText = def.value;
		slot.html.appendChild(div);
		
		// REMOVE 
		const removeDiv = document.createElement("div");
		removeDiv.innerText = "x";
		div.appendChild(removeDiv);
		
		removeDiv.onclick = function (e) {
			
			const storeConnection = this.store;
			
			this.html.style.borderLeft = "none";
			this.html.style.borderRight = "none";
			this.html.children[2].remove();
			
			this.defaults = null;
			
		}.bind(slot)
		
		
		// FIXED
		const fixedDiv = document.createElement("div");
		fixedDiv.className = "Fixed";
		fixedDiv.innerText = "!";
		div.appendChild(fixedDiv);

		fixedDiv.onclick = function (e) {
			const div = e.target.parentElement;
			if (div.classList.contains("F")) {
				div.classList.remove("F");
			} else {
				div.classList.add("F");
			}
		}
		
		return true;
		
	}
	
	getSlot(html) {
		
		if (!html) {
			return null;
		}
		
		for (const s of this.slots) {
			if (!s) {
				continue;
			}

			if (s.html === html) {
				return s;
			}
		}
		return null;
	}
	
	getSlotId(type, side, index) {
		for (const s of this.slots) {
			if (!s) {
				continue;
			}
			
			if (s.type === type && s.side === side && s.index === index) {
				return s;
			}
		}
		return null;
	}
	
}