let redesignPalettes = [
	[
		"rgba(69, 88, 85, 1)",
		"rgba(93, 136, 124, 1)",
		"rgba(198, 214, 210, 1)",
		"rgba(204, 146, 109, 1)",
		"rgba(237, 210, 196, 1)",
		"rgba(206, 191, 175, 1)",
		"rgba(245, 245, 245, 1)"
	],
	[
		"rgba(52, 52, 52, 1)",
		"rgba(38, 38, 38, 1)",
		"rgba(22, 22, 22, 1)",
		"rgba(231, 236, 242, 1)"
	],
	[
		"rgba(227, 52, 52, 1)",
		"rgba(213, 38, 38, 1)",
		"rgba(202, 22, 22, 1)",
		"rgba(0, 0, 0, 1)"
	],
	[
		"rgba(215, 227, 229, 1)",
		"rgba(183, 204, 210, 1)",
		"rgba(170, 147, 143, 1)",
		"rgba(218, 190, 190, 1)",
		"rgba(237, 216, 200, 1)",
		"rgba(238, 234, 231, 1)"
	],
	[
		"rgba(141, 135, 65, 1)",
		"rgba(101, 157, 189, 1)",
		"rgba(218, 173, 134, 1)",
		"rgba(188, 152, 106, 1)",
		"rgba(251, 238, 193, 1)"
	],
	[
		"rgba(93, 92, 97, 1)",
		"rgba(147, 142, 148, 1)",
		"rgba(115, 149, 174, 1)",
		"rgba(85, 122, 149, 1)",
		"rgba(177, 162, 150, 1)"
	],
	[
		"rgba(248, 233, 161, 1)",
		"rgba(247, 108, 108, 1)",
		"rgba(168, 208, 230, 1)",
		"rgba(55, 71, 133, 1)",
		"rgba(36, 48, 94, 1)"
	],
	[
		"rgba(161, 195, 209, 1)",
		"rgba(179, 155, 200, 1)",
		"rgba(240, 235, 244, 1)",
		"rgba(241, 114, 161, 1)",
		"rgba(230, 67, 152, 1)"
	],
	[
		"rgba(66, 133, 244, 1)",
		"rgba(92, 32, 24, 1)",
		"rgba(188, 70, 57, 1)",
		"rgba(212, 165, 154, 1)",
		"rgba(243, 224, 220, 1)"
	],
	[
		"rgba(251, 232, 166, 1)",
		"rgba(244, 151, 108, 1)",
		"rgba(48, 60, 108, 1)",
		"rgba(180, 223, 229, 1)",
		"rgba(210, 253, 255, 1)"
	],
	[
		"rgba(247, 136, 136, 1)",
		"rgba(243, 210, 80, 1)",
		"rgba(236, 236, 236, 1)",
		"rgba(144, 204, 244, 1)",
		"rgba(93, 162, 213, 1)"
	],
	[
		"rgba(0, 136, 122, 1)",
		"rgba(255, 204, 188, 1)",
		"rgba(255, 255, 255, 1)",
		"rgba(211, 227, 252, 1)",
		"rgba(119, 166, 247, 1)"
	],
	[
		"rgba(163, 188, 182, 1)",
		"rgba(57, 96, 61, 1)",
		"rgba(60, 64, 61, 1)",
		"rgba(218, 222, 212, 1)",
		"rgba(255, 255, 255, 1)"
	],
	[
		"rgba(81, 226, 245, 1)",
		"rgba(157, 249, 239, 1)",
		"rgba(237, 247, 246, 1)",
		"rgba(255, 168, 182, 1)",
		"rgba(162, 128, 137, 1)"
	],
	[
		"rgba(160, 210, 235, 1)",
		"rgba(229, 234, 245, 1)",
		"rgba(208, 189, 244, 1)",
		"rgba(132, 88, 179, 1)",
		"rgba(73, 77, 95, 1)"
	],
	[
		"rgba(138, 48, 127, 1)",
		"rgba(121, 167, 211, 1)",
		"rgba(104, 131, 188, 1)"
	],
	[
		"rgba(242, 103, 100, 1)",
		"rgba(255, 255, 255, 1)"
	],
	[
		"rgba(249, 218, 159, 1)",
		"rgba(94, 195, 247, 1)",
		"rgba(173, 174, 212, 1)",
		"rgba(174, 225, 251, 1)",
		"rgba(244, 199, 181, 1)",
		"rgba(194, 226, 198, 1)",
		"rgba(181, 243, 212, 1)",
		"rgba(148, 162, 248, 1)",
		"rgba(188, 223, 95, 1)"
	],
	[
		"rgba(100, 101, 165, 1)",
		"rgba(105, 117, 166, 1)",
		"rgba(243, 233, 107, 1)",
		"rgba(242, 138, 48, 1)",
		"rgba(240, 88, 55, 1)"
	],
	[
		"rgba(171, 166, 191, 1)",
		"rgba(89, 87, 117, 1)",
		"rgba(88, 62, 46, 1)",
		"rgba(241, 224, 214, 1)",
		"rgba(191, 152, 143, 1)"
	],
	[
		"rgba(25, 46, 91, 1)",
		"rgba(29, 101, 166, 1)",
		"rgba(114, 162, 192, 1)",
		"rgba(0, 116, 63, 1)",
		"rgba(242, 161, 4, 1)"
	],
	[
		"rgba(163, 88, 109, 1)",
		"rgba(92, 74, 114, 1)",
		"rgba(243, 176, 90, 1)",
		"rgba(244, 135, 75, 1)",
		"rgba(244, 106, 78, 1)"
	],
	[
		"rgba(128, 173, 215, 1)",
		"rgba(10, 189, 160, 1)",
		"rgba(235, 242, 234, 1)",
		"rgba(212, 220, 169, 1)",
		"rgba(191, 157, 122, 1)"
	],
	[
		"rgba(117, 35, 51, 1)",
		"rgba(4, 56, 163, 1)",
		"rgba(5, 50, 142, 1)",
		"rgba(4, 61, 140, 1)",
		"rgba(142, 92, 29, 1)"
	],
	[
		"rgba(108, 107, 116, 1)",
		"rgba(46, 48, 62, 1)",
		"rgba(145, 153, 190, 1)",
		"rgba(84, 103, 143, 1)",
		"rgba(33, 38, 36, 1)"
	],
	[
		"rgba(142, 211, 244, 1)",
		"rgba(228, 235, 242, 1)",
		"rgba(50, 141, 170, 1)",
		"rgba(138, 134, 131, 1)",
		"rgba(90, 77, 76, 1)"
	],
	[
		"rgba(56, 124, 163, 1)",
		"rgba(82, 173, 200, 1)",
		"rgba(85, 195, 158, 1)"
	],
	[
		"rgba(90, 36, 180, 1)",
		"rgba(170, 0, 255, 1)",
		"rgba(254, 0, 234, 1)"
	],
	[
		"rgba(82, 55, 59, 1)",
		"rgba(214, 61, 65, 1)",
		"rgba(236, 86, 51, 1)"
	],
	[
		"rgba(169, 180, 11, 1)",
		"rgba(255, 192, 0, 1)",
		"rgba(243, 140, 1, 1)"
	],
	[
		"rgba(152, 80, 109, 1)",
		"rgba(208, 96, 94, 1)",
		"rgba(255, 153, 76, 1)"
	],
	[
		"rgba(237, 200, 193, 1)",
		"rgba(224, 146, 145, 1)",
		"rgba(247, 172, 128, 1)",
		"rgba(247, 195, 135, 1)",
		"rgba(192, 186, 151, 1)"
	],
	[
		"rgba(241, 237, 169, 1)",
		"rgba(165, 191, 114, 1)",
		"rgba(75, 113, 62, 1)",
		"rgba(201, 201, 197, 1)",
		"rgba(223, 223, 221, 1)",
		"rgba(230, 231, 234, 1)"
	],
	[
		"rgba(127, 94, 163, 1)",
		"rgba(189, 101, 151, 1)",
		"rgba(250, 201, 197, 1)",
		"rgba(240, 230, 238, 1)",
		"rgba(157, 211, 247, 1)"
	],
	[
		"rgba(85, 102, 137, 1)",
		"rgba(128, 198, 152, 1)",
		"rgba(254, 240, 184, 1)",
		"rgba(240, 206, 112, 1)",
		"rgba(139, 88, 60, 1)"
	],
	[
		"rgba(234, 226, 214, 1)",
		"rgba(213, 195, 170, 1)",
		"rgba(134, 118, 102, 1)",
		"rgba(225, 184, 13, 1)"
	],
	[
		"rgba(38, 92, 0, 1)",
		"rgba(104, 162, 37, 1)",
		"rgba(179, 222, 129, 1)",
		"rgba(253, 255, 255, 1)"
	],
	[
		"rgba(161, 1, 21, 1)",
		"rgba(215, 44, 22, 1)",
		"rgba(240, 239, 234, 1)",
		"rgba(192, 178, 181, 1)"
	],
	[
		"rgba(48, 27, 40, 1)",
		"rgba(82, 54, 52, 1)",
		"rgba(182, 69, 44, 1)",
		"rgba(221, 197, 162, 1)"
	],
	[
		"rgba(216, 65, 47, 1)",
		"rgba(254, 122, 71, 1)",
		"rgba(252, 253, 254, 1)",
		"rgba(245, 202, 153, 1)"
	],
	[
		"rgba(41, 136, 188, 1)",
		"rgba(47, 73, 110, 1)",
		"rgba(244, 234, 222, 1)",
		"rgba(237, 140, 114, 1)"
	],
	[
		"rgba(244, 125, 74, 1)",
		"rgba(225, 49, 91, 1)",
		"rgba(255, 236, 92, 1)",
		"rgba(0, 141, 203, 1)"
	],
	[
		"rgba(133, 147, 174, 1)",
		"rgba(90, 78, 77, 1)",
		"rgba(126, 103, 94, 1)",
		"rgba(221, 162, 136, 1)"
	],
	[
		"rgba(252, 200, 117, 1)",
		"rgba(186, 168, 150, 1)",
		"rgba(230, 204, 181, 1)",
		"rgba(227, 139, 117, 1)"
	],
	[
		"rgba(255, 204, 187, 1)",
		"rgba(110, 181, 192, 1)",
		"rgba(0, 108, 132, 1)",
		"rgba(226, 232, 228, 1)"
	],
	[
		"rgba(68, 76, 92, 1)",
		"rgba(206, 90, 87, 1)",
		"rgba(120, 165, 163, 1)",
		"rgba(225, 177, 106, 1)"
	],
	[
		"rgba(213, 84, 72, 1)",
		"rgba(255, 165, 119, 1)",
		"rgba(249, 249, 255, 1)",
		"rgba(137, 110, 105, 1)"
	],
	[
		"rgba(52, 77, 144, 1)",
		"rgba(92, 197, 239, 1)",
		"rgba(255, 183, 69, 1)",
		"rgba(231, 85, 44, 1)"
	],
	[
		"rgba(255, 190, 189, 1)",
		"rgba(252, 252, 250, 1)",
		"rgba(51, 123, 174, 1)",
		"rgba(26, 64, 95, 1)"
	],
	[
		"rgba(72, 138, 153, 1)",
		"rgba(219, 174, 88, 1)",
		"rgba(77, 88, 91, 1)",
		"rgba(180, 180, 180, 1)"
	],
	[
		"rgba(37, 128, 57, 1)",
		"rgba(245, 190, 65, 1)",
		"rgba(49, 169, 184, 1)",
		"rgba(207, 55, 33, 1)"
	],
	[
		"rgba(238, 105, 63, 1)",
		"rgba(246, 148, 84, 1)",
		"rgba(252, 253, 254, 1)",
		"rgba(115, 159, 61, 1)"
	],
	[
		"rgba(161, 190, 149, 1)",
		"rgba(226, 223, 162, 1)",
		"rgba(146, 170, 199, 1)",
		"rgba(237, 87, 82, 1)"
	],
	[
		"rgba(72, 151, 216, 1)",
		"rgba(255, 219, 92, 1)",
		"rgba(250, 110, 89, 1)",
		"rgba(248, 160, 85, 1)"
	],
	[
		"rgba(193, 225, 220, 1)",
		"rgba(255, 204, 172, 1)",
		"rgba(255, 235, 148, 1)",
		"rgba(253, 212, 117, 1)"
	],
	[
		"rgba(76, 63, 84, 1)",
		"rgba(209, 53, 37, 1)",
		"rgba(242, 192, 87, 1)",
		"rgba(72, 104, 36, 1)"
	],
	[
		"rgba(80, 81, 96, 1)",
		"rgba(104, 130, 158, 1)",
		"rgba(174, 189, 56, 1)",
		"rgba(89, 130, 52, 1)"
	],
	[
		"rgba(244, 204, 112, 1)",
		"rgba(222, 122, 34, 1)",
		"rgba(32, 148, 139, 1)",
		"rgba(106, 177, 135, 1)"
	],
	[
		"rgba(241, 241, 242, 1)",
		"rgba(188, 186, 190, 1)",
		"rgba(161, 214, 226, 1)",
		"rgba(25, 149, 173, 1)"
	],
	[
		"rgba(154, 158, 171, 1)",
		"rgba(93, 83, 94, 1)",
		"rgba(236, 150, 164, 1)",
		"rgba(223, 225, 102, 1)"
	],
	[
		"rgba(154, 158, 71, 1)",
		"rgba(255, 255, 255, 1)",
		"rgba(92, 130, 26, 1)",
		"rgba(198, 209, 102, 1)"
	],
	[
		"rgba(45, 96, 124, 1)",
		"rgba(150, 139, 132, 1)",
		"rgba(253, 186, 26, 1)",
		"rgba(240, 157, 6, 1)",
		"rgba(122, 151, 44, 1)"
	],
	[
		"rgba(21, 150, 148, 1)",
		"rgba(9, 174, 165, 1)",
		"rgba(199, 232, 190, 1)",
		"rgba(212, 182, 148, 1)",
		"rgba(255, 111, 66, 1)",
		"rgba(137, 105, 122, 1)"
	],
	[
		"rgba(254, 221, 226, 1)",
		"rgba(219, 115, 148, 1)",
		"rgba(149, 76, 116, 1)",
		"rgba(255, 200, 117, 1)",
		"rgba(225, 247, 126, 1)",
		"rgba(187, 240, 238, 1)"
	],
	[
		"rgba(113, 77, 105, 1)",
		"rgba(160, 132, 157, 1)",
		"rgba(211, 179, 184, 1)",
		"rgba(227, 218, 231, 1)",
		"rgba(238, 236, 243, 1)"
	],
	[
		"rgba(186, 119, 53, 1)",
		"rgba(239, 208, 145, 1)",
		"rgba(133, 170, 197, 1)",
		"rgba(2, 79, 148, 1)",
		"rgba(5, 8, 20, 1)"
	],
	[
		"rgba(245, 162, 172, 1)",
		"rgba(227, 98, 113, 1)",
		"rgba(194, 68, 94, 1)",
		"rgba(145, 46, 60, 1)",
		"rgba(40, 42, 73, 1)",
		"rgba(171, 175, 189, 1)"
	],
	[
		"rgba(98, 161, 142, 1)",
		"rgba(190, 217, 212, 1)",
		"rgba(248, 243, 212, 1)",
		"rgba(254, 201, 71, 1)",
		"rgba(251, 151, 31, 1)"
	],
	[
		"rgba(177, 221, 224, 1)",
		"rgba(247, 230, 212, 1)",
		"rgba(255, 187, 150, 1)",
		"rgba(208, 92, 131, 1)",
		"rgba(200, 215, 222, 1)"
	],
	[
		"rgba(202, 158, 147, 1)",
		"rgba(250, 193, 138, 1)",
		"rgba(241, 159, 112, 1)",
		"rgba(49, 100, 81, 1)",
		"rgba(17, 60, 41, 1)",
		"rgba(9, 40, 34, 1)"
	],
	[
		"rgba(110, 160, 135, 1)",
		"rgba(217, 163, 89, 1)",
		"rgba(166, 205, 187, 1)",
		"rgba(225, 155, 165, 1)",
		"rgba(238, 200, 199, 1)",
		"rgba(245, 229, 151, 1)"
	],
	[
		"rgba(162, 185, 188, 1)",
		"rgba(178, 173, 127, 1)",
		"rgba(135, 143, 153, 1)",
		"rgba(107, 91, 149, 1)"
	],
	[
		"rgba(214, 203, 211, 1)",
		"rgba(236, 161, 166, 1)",
		"rgba(189, 206, 190, 1)",
		"rgba(173, 163, 151, 1)"
	],
	[
		"rgba(66, 133, 244, 1)",
		"rgba(251, 188, 5, 1)",
		"rgba(52, 168, 83, 1)",
		"rgba(234, 67, 53, 1)"
	],
	[
		"rgba(85, 172, 238, 1)",
		"rgba(41, 47, 51, 1)",
		"rgba(102, 117, 127, 1)",
		"rgba(204, 214, 221, 1)",
		"rgba(225, 232, 237, 1)",
		"rgba(255, 255, 255, 1)"
	],
	[
		"rgba(59, 89, 152, 1)",
		"rgba(139, 157, 195, 1)",
		"rgba(223, 227, 238, 1)",
		"rgba(247, 247, 247, 1)",
		"rgba(255, 255, 255, 1)"
	],
	[
		"rgba(246, 83, 20, 1)",
		"rgba(124, 187, 0, 1)",
		"rgba(0, 161, 241, 1)",
		"rgba(255, 187, 0, 1)"
	],
	[
		"rgba(15, 27, 7, 1)",
		"rgba(255, 255, 255, 1)",
		"rgba(92, 130, 26, 1)",
		"rgba(198, 209, 102, 1)"
	]
]

let redesignThemes = {
	"0":
	{
		paletteIndex: 0,
		state: {
			"--blockHeaderBackgroundColor": "rgba(245, 245, 245, 1)",
			"--blockHeaderFontColor": "rgba(245, 245, 245, 1)",
			"--blockHeaderBackgroundGradientPosX": "30px",
			"--blockHeaderBackgroundGradientPosY": "20px",
			"--blockHeaderBackgroundGradientColorBegin": "rgba(245, 245, 245, 1)",
			"--blockHeaderBackgroundGradientColorEnd": "rgba(245, 245, 245, 1)",
			"--blockHeaderBackgroundGradient": "radial-gradient(farthest-corner at var(--blockHeaderBackgroundGradientPosX) var(--blockHeaderBackgroundGradientPosY), var(--blockHeaderBackgroundGradientColorBegin), var(--blockHeaderBackgroundGradientColorEnd))",
			"--blockHeaderBackground": "var(--blockHeaderBackgroundGradient)",
			"--blockHeaderBackgroundColorFocused": "rgba(245, 245, 245, 1)",
			"--blockHeaderBackgroundGradientFocusedColorBegin": "rgba(245, 245, 245, 1)",
			"--blockHeaderBackgroundGradientFocusedColorEnd": "rgba(245, 245, 245, 1)",
			"--blockHeaderBackgroundGradientFocused": "radial-gradient(farthest-corner at var(--blockHeaderBackgroundGradientPosX) var(--blockHeaderBackgroundGradientPosY), var(--blockHeaderBackgroundGradientFocusedColorBegin), var(--blockHeaderBackgroundGradientFocusedColorEnd))",
			"--blockHeaderBackgroundFocused": "var(--blockHeaderBackgroundGradientFocused)",
			"--blockBodyBackground": "rgba(245, 245, 245, 1)",
			"--blockBodyFontColor": "rgba(245, 245, 245, 1)",
			"--blockFooterBackground": "rgba(245, 245, 245, 1)",
			"--blockFooterBackgroundFocused": "rgba(245, 245, 245, 1)",
			"--blockFooterFontColor": "rgba(245, 245, 245, 1)",
			"--blockFooterClipPathTemplate": "url(#clipPathTemplate2)",
			"--blockFooterClipPath": "none",
			"--blockBorderWidth": "4px",
			"--blockBorderRadius": "8px",
			"--blockBorderColor": "rgba(245, 245, 245, 1)",
			"--connectionPointSize": "7px",
			"--connectionPointFlowBackground": "rgba(245, 245, 245, 1)",
			"--connectionPointFlowBorderStyle": "solid",
			"--connectionPointFlowBorderWidth": "1px",
			"--connectionPointFlowBorderColor": "rgba(245, 245, 245, 1)",
			"--connectionPointDataBackground": "rgba(245, 245, 245, 1)",
			"--connectionPointDataBorderStyle": "solid",
			"--connectionPointDataBorderWidth": "1px",
			"--connectionPointDataBorderColor": "rgba(245, 245, 245, 1)",
			"--connectionPathFlowColor": "rgba(245, 245, 245, 1)",
			"--connectionPathFlowWidth": "2px",
			"--connectionPathFlowColorFocused": "rgba(245, 245, 245, 1)",
			"--connectionPathFlowWidthFocused": "2px",
			"--connectionPathDataColor": "rgba(245, 245, 245, 1)",
			"--connectionPathDataWidth": "2px",
			"--connectionPathDataColorFocused": "rgba(245, 245, 245, 1)",
			"--connectionPathDataWidthFocused": "2px",
			"--connectionPathColorHovered": "rgba(245, 245, 245, 1)",
			"--connectionPathWidthHovered": "12px",
			"--gridCellSize": "16px",
			"--gridCellColor": "rgba(245, 245, 245, 1)",
			"--gridCellBorderColor": "rgba(245, 245, 245, 1)",
			"--gridCellBorderSize": "1px",
			"--gridSectorSize": "128px",
			"--gridSectorBorderColor": "rgba(245, 245, 245, 1)",
			"--gridSectorBorderSize": "2px"
		}
	}

};