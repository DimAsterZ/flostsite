/* eslint-disable max-lines-per-function */

// Global editor application
var APP = {};

// eslint-disable-next-line no-unused-vars
function init() {
	
	console.log("Start!");
	
	APP.specialTypes = [
		"Handler",
		"HandlerWithParams"
	];
	
	APP.index = -1;
	APP.script = script;
	APP.Logic = new Logic();
	APP.UI = new UI();
	APP.UI.createTop();
	APP.Logic.parseScript();
	
	APP.UI.createStoreBlock();
	Redesigner.instance.redesign();
	APP.UI.createLibrares();
	
	console.log("Complited!");
	
}

/**
 * 
 * class LOGIC
 * 
 **/
class Logic {
	
	constructor() {
		
		this.nodes = {};			// List of BLOCKS {ID|Object}
		this.connections = {};		// List of LINES {ID|Object}
		
		this.stores = {};				// Real Store
		this.storesList = [];			// Names of Store
		this.storesBlocks = {};			// Blocks For Store Work
		this.storesConnections = [];	// Connections For Store Work
		
		// {NODE|array nodes}
		this.logicBlocksStructs = [];	//START HENDLERS
		
		

		document.body.addEventListener("drop", this.drop.bind(this));
		document.body.addEventListener("dragover", this.dragover.bind(this));

		
	}
	
	parseScript() {
		
		const ids = [];
		
		// PARSE STORE LIST
		for (const name in APP.script.store) {
			const value = APP.script.store[name];
			
			this.stores[name] = value;
			this.storesList.push(name);
			
		}
		
		
		const container = document.getElementById("container");
		// PARSE NODES
		for (const n of APP.script.nodes) {
			
			// console.log("CreateNode:", n);
			
			if (APP.index < n.id) {
				APP.index = n.id;
			}
			
			// CHECK STORE BLOCK
			if (n.type === "FromStoreElement" || n.type === "ToStoreElement") {
				this.appendStoreBlock(n);
				ids.push(n.id);
				continue;
			}
			
			// CREATE NODE
			const o = new BasicNode(n);
			this.nodes[n.id] = o;
			APP.UI.nodesScene.appendChild(o.htmlElement);
			
			// CHECK START BLOCKs
			if (APP.specialTypes.indexOf(n.type) !== -1) {
				o.htmlElement.classList.add("StartNodes");
				o.x = 10;
				
				// HANDLER VIEW AREA
				const nodesArea = {};
				nodesArea.root = o;
				nodesArea.nodes = [];
				nodesArea.y1 = 9e10;
				nodesArea.y2 = 0;
				nodesArea.html = document.createElement("div");
				nodesArea.html.className = "ViewArea";
				container.appendChild(nodesArea.html);
				this.logicBlocksStructs.push(nodesArea);
				
			}
			
		}
		
		
		
		// PARSE CONNECTION LINES
		for (const c of APP.script.connections) {
			
			// console.log("CreateConnection:", c);
			
			// CHECK STORE CONNECTION
			const id1 = c.dest.nodeID;
			const id2 = c.source.nodeID;
			if (ids.indexOf(id1) !== -1 || ids.indexOf(id2) !== -1) {
				
				// Check on VALID connection
				const n1 = this.nodes[id1];
				const n2 = this.nodes[id2];
				if (!n1 && !n2) {
					console.error("Connection ", c, "not connected!");
					continue;
				}
				
				let result = false
				
				// Add Stores
				if (n1) {
					result = n1.appendStore(c);
				} else if (n2) {
					result = n2.appendStore(c);
				}
				
				if (!result) {
					continue;
				}
				
				this.storesConnections.push(c);
				continue;
				
			}
			
			// CREATE CONNECTION
			const o = new BasicConnection(c);
			this.connections[o.id] = o;
			o.staticCreate();
			o.updatePositions();
			APP.UI.nodesScene.appendChild(o.htmlElement);
			
		}
		
	
		
		// PARSE NODES VIEW AREAS
		for (const area of this.logicBlocksStructs) {
			// Добавление в обьект всей цепочки блоков
			this.checkViewStruct(area.root, area);
		}
		
		for (const area of this.logicBlocksStructs) {
			// Расчет позиций по умолчанию
			let offset = 100;
			area.y1 = 9e10;
			area.y2 = -9e10;
			for (let node of area.nodes) {
				area.y1 = Math.min(area.y1, node.htmlElement.offsetTop);
				area.y2 = Math.max(area.y2, node.htmlElement.offsetTop + node.htmlElement.offsetHeight);
			}
			area.y1 -= offset;
			area.y2 += offset;
			area.html.style.top =area.y1 + "px";
			area.html.style.height = (area.y2 - area.y1) + "px";
		}
		
		
		// Сортировка массива по Y координате 
		this.logicBlocksStructs.sort(this.sortFunction);
		
		this.changeAreaColor();
		
		// Корректировка 
		this.postCorrect();
		
		
	}
	
	
	sortFunction(obj1, obj2) {
		if (!obj1 || !obj2) {
			return 0;
		}
		return (obj1.y1 - obj2.y1);
	}
	
	checkViewStruct(node, area) {
		
		if (node.viewArea) {
			return;
		}
		node.viewArea = area;
		area.nodes.push(node);
		for (const connection of node.lines) {
			if (connection.type !== "flow" || connection.nodeID2 !== node.id) {
				continue;
			}
			
			const nodeNext = this.nodes[connection.nodeID1];
			this.checkViewStruct(nodeNext, area);
			
		}
	}

	updateNodeViewAreas(area) {

		
		if (!area) {
			return;
		}
		
		let index = this.logicBlocksStructs.indexOf(area);
		if (index === -1) {
			return;
		}
		
		// RECALCULATE AREA
		let offset = 100;
		let oldY1 = area.y1;
		let oldY2 = area.y2;
		area.y1 = 9e10;
		area.y2 = -9e10;

		for (let node of area.nodes) {
			area.y1 = Math.min(area.y1, node.htmlElement.offsetTop);
			area.y2 = Math.max(area.y2, node.htmlElement.offsetTop + node.htmlElement.offsetHeight);
		}
		area.y1 -= offset;
		area.y2 += offset;
		area.html.style.top = area.y1 + "px";
		area.html.style.height = (area.y2 - area.y1) + "px";
		
		let dY1 = oldY1 - area.y1;
		let dY2 = oldY2 - area.y2;
		
		// CORRECT UP
		if (dY1 !== 0) {
			for (let i = index - 1; i >= 0; i--) {
				let _area = this.logicBlocksStructs[i];
				for (let node of _area.nodes) {
					node.y -= dY1;
					node.htmlElement.style.top = node.htmlElement.offsetTop - dY1 + "px";
					for (const l of node.lines) {
						l.updatePositions();
					}
				}
				_area.y1 -= dY1;
				_area.y2 -= dY1;
				_area.html.style.top = _area.y1 + "px";
				_area.html.style.height = (_area.y2 - _area.y1) + "px";
				
			}
		}
		
		// CORRECT DOWN
		if (dY2 !== 0) {
			let count = this.logicBlocksStructs.length;
			for (let i = index + 1; i < count; i++) {
				let _area = this.logicBlocksStructs[i];
				for (let node of _area.nodes) {
					node.y -= dY2;
					node.htmlElement.style.top = node.htmlElement.offsetTop - dY2 + "px";
					for (const l of node.lines) {
						l.updatePositions();
					}
				}
				_area.y1 -= dY2;
				_area.y2 -= dY2;
				_area.html.style.top = _area.y1 + "px";
				_area.html.style.height = (_area.y2 - _area.y1) + "px";
			}
		}

	}
	
	
	addUpdatePos(delta) {
		let _area = this.logicBlocksStructs[i];
		for (let node of _area.nodes) {
			node.y -= dY2;
			node.htmlElement.style.top = node.htmlElement.offsetTop - dY2 + "px";
			for (const l of node.lines) {
				l.updatePositions();
			}
		}
		_area.y1 -= dY2;
		_area.y2 -= dY2;
		_area.html.style.top = _area.y1 + "px";
		_area.html.style.height = (_area.y2 - _area.y1) + "px";
	}

	
	// correctPositions(area) {
		
	// 	for (const _area of this.logicBlocksStructs) {
			
	// 		let d = 0;
			
	// 		if (_area.y1 < area.y1 && _area.y2 > area.y1) {		//  UP
	// 			d = _area.y2 - area.y1;
	// 		}
	// 		else
	// 		if (area.y1 < _area.y1 && area.y2 > _area.y1) {		//  DOWN
	// 			d = area.y2 - _area.y1;
	// 		}
			
	// 		for (let node of _area.nodes) {
	// 			node.y -= d;
	// 			node.htmlElement.style.top = node.htmlElement.offsetTop - d + "px";
				
	// 			for (const l of node.lines) {
	// 				l.updatePositions();
	// 			}
	// 		}
	// 		this.updateNodeViewAreas(_area);
			
	// 	}
	// 	this.postCorrect();
	// }
	

	postCorrect() {
		
		let count = this.logicBlocksStructs.length - 1;
		for (let i = 0; i < count; i++) {
			let o1 = this.logicBlocksStructs[i];
			let o2 = this.logicBlocksStructs[i + 1];
			
			let d = o2.y1 - o1.y2;
			
			o2.y1 -= d;
			o2.y2 -= d;
			o2.html.style.top = o2.y1 + "px";
			o2.html.style.height = (o2.y2 - o2.y1) + "px";
			for (let node of o2.nodes) {
				node.y -= d;
				node.htmlElement.style.top = node.htmlElement.offsetTop - d + "px";
				for (const l of node.lines) {
					l.updatePositions();
				}
			}
			
		}
	}
	
	parseLibrary() {
		
		
		
	}

	appendStoreBlock(data) {
		this.storesBlocks[data.id] = data;
		const name = data.params[0].ElementName;
		if (this.storesList.indexOf(name) === -1) {
			this.stores[name] = "";
			this.storesList.push(name);
		}
	}
	
	clearScript() {
		
		script = null;
		APP.script = null;
		
		for (const n_id in this.nodes) {
			const n = this.nodes[n_id];
			this.removeObject(n);
		}
		
		for (const n_id in this.connections) {
			const n = this.connections[n_id];
			this.removeObject(n);
		}
		
	}
	
	removeObjects(os) {
		if (!os) {
			for (const o of APP.UI.focused) {
				this.removeObject(o);
			}
		} else {
			for (const o of os) {
				this.removeObject(o);
			}
		}
	}
	
	removeObject(object) {
		
		if (!object) {
			return;
		}
		
		if (this.nodes[object.id]) {
			if (object.type === "Handler" || object.type === "HandlerWithParams") {
				return;
			}
			delete this.nodes[object.id];
		} else if (this.connections[object.id]) {
			delete this.connections[object.id];
		}
		
		object.destroy();
		
	}
	
	removeStoreConnection(data) {
		
		if (!data) {
			return;
		}
		
		const b_id_1 = data.dest.nodeID;
		const b_id_2 = data.source.nodeID;
		const index = this.storesConnections.indexOf(data);
		this.storesConnections.splice(index, 1);
		
		if (this.storesBlocks[b_id_1]) {
			delete this.storesBlocks[b_id_1];
		} else if (this.storesBlocks[b_id_2]) {
			delete this.storesBlocks[b_id_2];
		}
		
	}
	
	saveScript() {
		
		// SAVE OBJECT
		const object = {};
		object.nodes = [];
		object.connections = [];
		
		// SAVE NODES
		for (const nodeName in this.nodes) {
			if (!nodeName) {
				continue;
			}
			const node = this.nodes[nodeName];
			object.nodes.push(this.generateNode(node));
			
		}
		
		// SAVE STORE NODES
		for (const sn_id in this.storesBlocks) {
			if (!sn_id) continue;
			
			const sn = this.storesBlocks[sn_id];
			
			if (!sn) continue;
			object.nodes.push(sn);
		}
		
		// SAVE CONNECTION
		for (const connectionID in this.connections) {
			if (!connectionID) {
				continue;
			}
			const connection = this.connections[connectionID];
			if (!connection) {
				continue;
			}
			const oConnection = {};
			oConnection.type = connection.type;
			
			if (connection.pid !== undefined)
				oConnection.pid = connection.pid;
			
			oConnection.source = {
			"nodeID": connection.nodeID2,
			"index": connection.index2
			};
			oConnection.dest = {
			"nodeID": connection.nodeID1,
			"index": connection.index1
			};
			object.connections.push(oConnection);
		}
		
		// SAVE STORE CONNECTION 
		for (const sn of this.storesConnections) {
			if (!sn) continue;
			object.connections.push(sn);
		}
		
		// SAVE STORE VALUES
		object.store = {};
		for (const name in this.stores) {
			const value = this.stores[name];
			object.store[name] = value;
		}
		
		script = object;
		
		// this.saveFile();
		// console.log(JSON.stringify(object));
		
	}
	
	dublicate(event, objects) {
		
		if (!objects) {
			return;
		}
		
		for (const object of objects) {
			
			if (!object.id) {
				return;
			}
			
			const dublicateNode = this.generateNode(object);
			dublicateNode.id = ++APP.index;
			dublicateNode.x = APP.UI.getSceneOffset(event, 0);
			dublicateNode.y = APP.UI.getSceneOffset(event, 1);
			const o = new BasicNode(dublicateNode);
			this.nodes[dublicateNode.id] = o;
			APP.UI.nodesScene.appendChild(o.htmlElement);
			
		}
	}
	
	copy(objects) {
		
	}
	
	paste() {
		
	}
	
	
	loadFile(event) {
		
		const file = event.target.files[0];
		
		var reader = new FileReader();
		reader.addEventListener('load', function (e) {
			const s = e.target.result;
			const obj = JSON.parse(s);
			this.clearScript();
			APP.script = obj;
			script = obj;
			this.parseScript();
			
		}.bind(this));
		reader.readAsBinaryString(file);
		
	}
	
	
	saveFile() {
		
		const dt = new Date();
		const dd = "" + dt.toDateString();
		let tt = "" + dt.toLocaleTimeString();
		tt = tt.replace(":", ".");
		tt = tt.replace(":", ".");
		const name = "save_" + dd + "_" + tt;
		const data = JSON.stringify(script);
		
		var element = document.createElement('a');
		element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
		element.setAttribute('download', name);
		element.style.display = 'none';
		document.body.appendChild(element);
		element.click();
		document.body.removeChild(element);
		
	}
	
	
	getArraySlots(obj) {
		const result = [];
		for (const slotName in obj) {
			if (!slotName) {
				continue;
			}
			const slot = obj[slotName];
			if (!slot) {
				continue;
			}
			result.push(slotName);
		}
		return result;
	}

	generateNode(node) {
		const oNode = {};
		if (node.type === "JavaScriptBlock") {
			oNode.name = node.name;
		}
		oNode.type = node.type;
		oNode.id = node.id;
		if (node.pid !== null && node.pid !== undefined) {
			oNode.pid = node.pid;
		}
		if (node.cid !== null && node.cid !== undefined) {
			oNode.cid = node.cid;
		}
		oNode.x = node.htmlElement.offsetLeft;
		oNode.y = node.htmlElement.offsetTop;
		if ("defParam" in node) {
			oNode.params = node.defParam;
		}
		oNode.Inputs = {
			"flow": this.getArraySlots(node.flowIn),
			"data": this.getArraySlots(node.dataIn)
		};
		oNode.Outputs = {
			"flow": this.getArraySlots(node.flowOut),
			"data": this.getArraySlots(node.dataOut)
		};
		const i_Defaults = [];
		const o_Defaults = [];
		for (const s of node.slots) {
			if (s.defaults) {
				if (s.side === "OUT") {
					o_Defaults.push(s.defaults);
				} else {
					i_Defaults.push(s.defaults);
				}
			}
		}
		if (i_Defaults.length || o_Defaults.length) {
			oNode.Defaults = {};
			if (i_Defaults.length) {
				oNode.Defaults.Inputs = i_Defaults;
			}
			if (o_Defaults.length) {
				oNode.Defaults.Outputs = o_Defaults;
			}
		}
		return oNode;
	}
	
	
	changeAreaColor() {
		// Изменение цветов
		let count = this.logicBlocksStructs.length;
		for (let i = 0; i < count; i++) {
			let o1 = this.logicBlocksStructs[i];

			if (i % 2 == 0) {

				o1.html.style.background = "var(--evenViewAreaBackground)";//redesign
				// o1.html.style.background = "#e9d0b2a1";
			} else {

				o1.html.style.background = "var(--oddViewAreaBackground)";//redesign
				// o1.html.style.background = "#00c5ffa1";
				// o1.html.style.background = "#d3b9a0a1";
			}
		}
	}
	


	drop(event) {
		
		// console.log("drop", event.target, event);
		
		
		if (!this.DROP_OBJ) {
			return;
		}
		
		let libObj = APP.Logic.LibData[this.DROP_OBJ];
		if (!libObj) {
			return;
		}
		
		let startBlock = false;

		if (libObj.type == "HandlerWithParams" || libObj.type == "Handler") {
			startBlock = true;
			if (event.target.className == "ViewArea") {
				return;
			}
		}
		
		
		let vec = APP.UI.translatePosTo(event.clientX, event.clientY);
		console.log(vec);
		
		libObj.id = ++APP.index;
		libObj.x = vec[0];
		libObj.y = vec[1];
		
		let new_node = new BasicNode(libObj);
		this.nodes[new_node.id] = new_node;
		APP.UI.nodesScene.appendChild(new_node.htmlElement);
		
		
		let area = null;
		if (!startBlock) {
			
			for (let _area of this.logicBlocksStructs) {
				if (_area.html === event.target) {
					area = _area;
				}
			}
			
			
		} else {
			// NEW AREA
			new_node.htmlElement.classList.add("StartNodes");
			new_node.x = 10;

			// HANDLER VIEW AREA
			area = {};
			area.root = new_node;
			area.nodes = [];
			area.y1 = vec[1];
			area.y2 = vec[1];
			area.html = document.createElement("div");
			area.html.className = "ViewArea";
			container.appendChild(area.html);
			this.logicBlocksStructs.push(area);
			
			// Сортировка массива по Y координате 
			this.logicBlocksStructs.sort(this.sortFunction);
			
			area.y1 = Math.min(area.y1, new_node.htmlElement.offsetTop);
			area.y2 = Math.max(area.y2, new_node.htmlElement.offsetTop + new_node.htmlElement.offsetHeight);
			
			area.html.style.top = area.y1 + "px";
			area.html.style.height = (area.y2 - area.y1) + "px";
			
			this.changeAreaColor();
			
			// Корректировка 
			this.postCorrect();
			// this.updateNodeViewAreas
			
		}
		
		if (area == null) {
			this.removeObject(new_node);
			return;
		}
		
		
		new_node.viewArea = area;
		area.nodes.push(new_node);
		
		this.updateNodeViewAreas(this.logicBlocksStructs[0]);
		this.updateNodeViewAreas(area);
		
		//redesign
		if(Redesigner.instance.enabled) {
			Redesigner.instance.removeBlockIndents();
		}
	}
	

	dragover(event) {
		event.preventDefault();
	}
	
	
}