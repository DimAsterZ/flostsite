/* eslint-disable max-lines-per-function */
/* eslint-disable no-continue */
/* eslint-disable no-console */
/* exported globalShiftX init*/
/* global script BasicNode BasicConnection Slot*/
'use strict';

class UI {

	constructor() {

		this.nodesScene = document.getElementById("container");
		this.userView = document.getElementById("root");

		this.selecedArea = null;
		this.hovered = null;
		this.focused = [];

		this.mX = 0;
		this.mY = 0;
		this.moveEvent = null;

		this.camX = 0;
		this.camY = 0;
		this.camZ = 1;

		this.movingScene = false;

		this.connectBrowserEvents();

		// MAKE SVG CONTAINER
		// http://www.w3.org/2000/svg
		this.namespaceURL = decodeURIComponent(escape(window.atob("aHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmc=")));

		this.svgContainer = document.createElementNS(this.namespaceURL, "svg");
		this.svgContainer.classList.add("SVG_CONTAINER");
		this.svgContainer.style.overflow = "visible";

		this.storeContainer = null;
		this.storeSelected = null;
		this.nodeSlot = null;


		// CONTAINER HANDLERS
		this.handlersView = document.createElement("div");
		this.handlersView.className = "ViewView";
		this.nodesScene.appendChild(this.handlersView);



		// TRY SVG EDITOR: https://yqnn.github.io/svg-path-editor/
		// BLOCKS PATH TEMPLATE
		let _clipPathTemlate1 = document.createElementNS(this.namespaceURL, "clipPath");
		_clipPathTemlate1.id = "clipPathTemplate1";
		let _pathTemlate1 = document.createElementNS(this.namespaceURL, "path");
		// _pathTemlate1.setAttribute("d", `M 0 0 L 0 41 L 200 41 L 180 20 C 160 0 160 0 140 0 L 0 0 Z`);
		_pathTemlate1.setAttribute("d", `M 0 0 L 0 41 L 200 41 L 200 41 C 122 0 122 0 122 0 L 0 0 Z`);
		_pathTemlate1.setAttribute("d", `M 0 0 L 0 20 L 200 20 L 200 20 C 122 0 122 0 122 0 L 0 0 Z`);

		this.svgContainer.appendChild(_clipPathTemlate1);
		_clipPathTemlate1.appendChild(_pathTemlate1);

		//BOTTOM
		let _clipPathTemlate2 = document.createElementNS(this.namespaceURL, "clipPath");
		_clipPathTemlate2.id = "clipPathTemplate2";
		let _pathTemlate2 = document.createElementNS(this.namespaceURL, "path");
		// _pathTemlate2.setAttribute("d", `M 0 0 L 200 0 L 200 20 L 30 20 C 20 20 20 20 10 10 L 0 0`);
		_pathTemlate2.setAttribute("d", `M 0 0 L 200 0 L 200 20 L 78 19 C 78 19 78 19 0 0 L 0 0`);
		this.svgContainer.appendChild(_clipPathTemlate2);
		_clipPathTemlate2.appendChild(_pathTemlate2);

	}

	connectBrowserEvents() {
		document.addEventListener("mousedown", this.mouseDown.bind(this));
		document.addEventListener("mousemove", this.mouseMove.bind(this));
		document.addEventListener("mouseup", this.mouseUp.bind(this));
		// document.addEventListener("mousewheel", 	this.mouseWheel.bind(this));//redesign
		document.addEventListener("keydown", this.keyDown.bind(this));
	}

	mouseDown(event) {

		// CHECK HOVER OBJECT
		this.hovered = this.checkHover(event);


		let targetClass = event.target.className;

		// START SELECTED AREA
		if (!this.selecedArea &&
			!this.hovered &&
			event.which === 1 &&
			// (event.target == this.nodesScene ||	event.target == document.body)) {
			(targetClass == "container" || targetClass == "ViewView" || targetClass == "ViewArea" ||
				event.target == document.body)) {

			this.selecedArea = document.createElement("div");
			this.selecedArea.classList.add("SelectedArea");

			this.selecedArea.sPX = this.getSceneOffset(event, 0);
			this.selecedArea.sPY = this.getSceneOffset(event, 1);

			// document.body.appendChild(this.selecedArea);
			this.nodesScene.appendChild(this.selecedArea);

			this.clearFocudes();

		}

		// FOCUSED ONE OBJECT
		if (this.hovered && event.which === 1) {

			if (this.focused.indexOf(this.hovered) === -1) {
				this.clearFocudes();

				this.focused.push(this.hovered);
				this.hovered.focused(true);
			}

			if (this.focused.length === 1) {
				this.focused[0].mouseDown(event);
			} else {
				this.msoX = APP.UI.getSceneOffset(event, 0);
				this.msoY = APP.UI.getSceneOffset(event, 1);
			}

		}



		// START MOVING SCENE
		if (event.which === 2) {
			this.movingScene = true;
			this.movingX = event.clientX / this.camZ;
			this.movingY = event.clientY / this.camZ;
			return;
		}



	}

	mouseMove(event) {

		this.moveEvent = event;

		// CHECK HOVER OBJECT
		this.hovered = this.checkHover(event);

		// SELECTED AREA RESIZE
		if (this.selecedArea) {

			const x = this.getSceneOffset(event, 0);
			const y = this.getSceneOffset(event, 1);

			this.selecedArea.style.top = Math.min(this.selecedArea.sPY, y) + "px";
			this.selecedArea.style.left = Math.min(this.selecedArea.sPX, x) + "px";
			this.selecedArea.style.width = Math.abs(this.selecedArea.sPX - x) + "px";
			this.selecedArea.style.height = Math.abs(this.selecedArea.sPY - y) + "px";

			return;
		}

		// MOVING SCENE PRECESS
		if (this.movingScene) {
			this.moveScene(event);
		}

		// MOVE FOCUSED OBJECTS
		if (this.focused.length) {

			let node = this.focused[0];
			if (this.focused.length === 1) {
				node.mouseMove(event);
			} else if (event.which === 1) {

				const dx = this.msoX - APP.UI.getSceneOffset(event, 0);
				const dy = this.msoY - APP.UI.getSceneOffset(event, 1);

				for (const o of this.focused) {
					if (!o) continue;
					o.htmlElement.style.left = o.htmlElement.offsetLeft - dx + "px";
					o.htmlElement.style.top = o.htmlElement.offsetTop - dy + "px";

					if (o.htmlElement.offsetLeft < 300) {
						o.htmlElement.style.left = "300px";
					}

					for (const l of o.lines) {
						l.updatePositions();
					}
				}

				this.msoX = APP.UI.getSceneOffset(event, 0);
				this.msoY = APP.UI.getSceneOffset(event, 1);
			}
			APP.Logic.updateNodeViewAreas(node.viewArea);
		}

		this.mX = event.clientX;
		this.mY = event.clientY;

	}

	mouseUp(event) {

		// CHECK SELECTED AREA
		if (this.selecedArea) {

			const saX = this.selecedArea.offsetLeft;
			const saY = this.selecedArea.offsetTop;
			const saW = this.selecedArea.offsetWidth;
			const saH = this.selecedArea.offsetHeight;

			for (const nId in APP.Logic.nodes) {
				const n = APP.Logic.nodes[nId];
				if (!n) continue;
				const nX = n.htmlElement.offsetLeft;
				const nY = n.htmlElement.offsetTop;
				const nW = n.htmlElement.offsetWidth;
				const nH = n.htmlElement.offsetHeight;
				if (saX < nX && saY < nY && saX + saW > nX + nW && saY + saH > nY + nH) {
					this.focused.push(n);
					n.focused(true);
				}
			}

		}

		// MOVE FOCUSED OBJECTS
		if (this.focused.length) {

			if (this.focused.length === 1) {
				this.focused[0].mouseUp(event);
			}

		}

		this.movingScene = false;
		if (this.selecedArea) {
			this.selecedArea.remove();
			this.selecedArea = null;
		}

	}

	mouseWheel(event) {

		// if (event.target !== this.nodesScene && event.target !== document.body) {
		// 	return;
		// }

		let deltaValue = (event.deltaY || event.delta || event.wheelDeltaY || event.wheelDelta);
		let delta = (deltaValue < 0) ? -1 : 1;

		this.camZ += delta * 0.1;
		if (this.camZ <= 0.1) {
			this.camZ = 0.1;
		} else if (this.camZ > 2) {
			this.camZ = 2;
		}


		let w = window.innerWidth / 2;
		let h = window.innerHeight / 2;
		let _x = (event.clientX - w) / this.camZ + w + this.camX - (w / this.camZ);
		let _y = (event.clientY - h) / this.camZ + h + this.camY - (h / this.camZ);

		// let vec = this.translatePosToCenter(event.clientX, event.clientY);
		this.camX = _x;
		this.camY = _y;

		this.checkScenePosition(0, 0);
		this.updateScene();

	}

	keyDown(event) {

		if (event.repeat) {
			return;
		}
		const keyCodeS = 83;
		const keyCodeD = 68;
		const keyCodeDelete = 46;
		const keyCodeTab = 9;

		// SAVE
		if (event.ctrlKey && event.keyCode === keyCodeS) {
			event.preventDefault();
			APP.Logic.saveScript();
			APP.Logic.saveFile();
		}
		// DUBLICATE
		if (event.ctrlKey && event.keyCode === keyCodeD) {
			event.preventDefault();
			APP.Logic.dublicate(this.moveEvent, this.focused);
		}
		// DELETE
		if (event.keyCode === keyCodeDelete) {
			event.preventDefault();
			APP.Logic.removeObjects();
		}
		// TAB
		if (event.keyCode === keyCodeTab) {
			event.preventDefault();
			this.view();
		}

	}


	moveScene(event) {
		const dx = event.clientX - this.mX;
		const dy = event.clientY - this.mY;
		this.checkScenePosition(dx, dy);
		this.updateScene();

	}

	checkScenePosition(dx, dy) {
		// GET UP LEFT POINT
		let px = APP.Logic.logicBlocksStructs[0].html.offsetLeft;
		let py = APP.Logic.logicBlocksStructs[0].html.offsetTop;

		let vec = this.translatePosFrom(px, py);
		let controlX = vec[0];
		let controlY = vec[1];

		this.camX -= dx / this.camZ;
		this.camY -= dy / this.camZ;

		// CHECK ON UP LEFT POINT
		if (this.camX < controlX) {
			this.camX = controlX;
		}
		if (this.camY < controlY) {
			this.camY = controlY;
		}

	}

	view() {
		if (this.userView.style.display === "none") {
			this.userView.style.display = "inline-block"
		} else {
			this.userView.style.display = "none"
		}
	}

	getSceneOffset(e, v) {
		if (v === 0) {
			const w = window.innerWidth / 2;
			return (e.clientX - w) / this.camZ + w + this.camX;
		} else if (v === 1) {
			const h = window.innerHeight / 2;
			return (e.clientY - h) / this.camZ + h + this.camY;
		}
	}

	updateScene() {
		this.nodesScene.style.transform = `scale(${this.camZ}) translate(${(-this.camX)}px, ${(-this.camY)}px)`;
	}

	checkHover(event) {

		const path = event.path || (event.composedPath && event.composedPath());

		for (const n of path) {

			if (!n) {
				continue;
			}

			if (!n.id) {
				continue;
			}

			if (n.id.indexOf("N_") !== -1) {
				const id = n.id.substr(2);
				return APP.Logic.nodes[id];
			} else if (n.id.indexOf("C1_") !== -1 || n.id.indexOf("C2_") !== -1 || n.id.indexOf("P1_") !== -1 || n.id.indexOf("P2_") !== -1) {
				const id = n.id.substr(3);
				return APP.Logic.connections[id];
			}

		}

		return null;
	}


	clearFocudes() {
		for (const o of this.focused) {
			o.focused(false);
		}
		this.focused = [];
	}


	createTop() {

		// TOP LINE CONTAINER
		this.topBar = document.createElement("div");
		this.topBar.id = "TopBar";
		document.body.appendChild(this.topBar);

		// HIDDEN INPUT
		const inp = document.createElement("input");
		inp.style.display = "none";
		inp.setAttribute("type", "file");
		inp.multiple = false;
		inp.style.display = "none";
		inp.addEventListener("change", APP.Logic.loadFile.bind(APP.Logic));


		this.b1 = this.createBtn(this.topBar, "OPEN", function f() {
			this.b1.firstElementChild.click();
		}.bind(this));

		this.b2 = this.createBtn(this.topBar, "SAVE", function f2() {
			APP.Logic.saveScript();
			APP.Logic.saveFile();
		}.bind(this));

		this.b3 = this.createBtn(this.topBar, "VIEW", this.view.bind(this));

		this.b5 = this.createBtn(this.topBar, "STORE", this.getStoreList.bind(this));

		this.b4 = this.createBtn(this.topBar, "RUN", function f3() {
			// APP.Logic.saveScript();
			// APP.Logic.saveFile();
			APP.Logic.saveScript();
			resetAll();
		}.bind(this));

		this.b1.appendChild(inp);

		this.searchInput = document.createElement("input");
		this.topBar.appendChild(this.searchInput);

		let that = this;

		this.b6 = this.createBtn(this.topBar, "FORM", function f3() {
			// APP.Logic.saveScript();
			// APP.Logic.saveFile();

			let formEditorContainer = document.getElementById('formEditorContainer');

			if (that.b6.textContent == "FORM") {

				if (formEditorContainer == null) {

					formEditorContainer = document.createElement('div');

					formEditorContainer.id = "formEditorContainer";

					let style = formEditorContainer.style;

					style.position = "absolute";
					style.top = "20px";
					style.width = "100%";
					style.height = "calc(100% - 25px)";
					style.zIndex = "10";
					style.background = "white";
					// style.background = "var(--gridCellColor)";

					document.body.appendChild(formEditorContainer);

					let formEditorApp = new FormEditorApp();

					formEditorApp.init(formEditorContainer);
				}

				formEditorContainer.style.display = "block";

				that.b6.textContent = "SCRIPT";

			} else {

				formEditorContainer.style.display = "none";

				that.b6.textContent = "FORM";
			}



		}.bind(this));

		this.b7 = this.createBtn(this.topBar, "<", function f3() {

			let redesigner = Redesigner.instance;

			if (!redesigner.enabled)
				return;

			let index = redesigner.currentThemeIndex;

			if (index == 0)
				return;

			redesigner.setCurrentTheme(index - 1);

		}.bind(this));

		this.b7.style.width = '29px';

		this.b8 = this.createBtn(this.topBar, "THEME", function f3() {

			let redesigner = Redesigner.instance;

			if (!redesigner.enabled)
				return;

			if (redesigner.themeConfigMenu.style.display == "none")
				redesigner.themeConfigMenu.style.display = "block";
			else
				redesigner.themeConfigMenu.style.display = "none";

		}.bind(this));

		this.b9 = this.createBtn(this.topBar, ">", function f3() {

			let redesigner = Redesigner.instance;

			if (!redesigner.enabled)
				return;

			let index = redesigner.currentThemeIndex;

			if (index == redesigner.themeList.length - 1)
				return;

			redesigner.setCurrentTheme(index + 1);

		}.bind(this));

		this.b9.style.width = '29px';

		this.b10 = this.createBtn(this.topBar, "EXPAND DEFAULTS", function f3() {

			if (that.b10.textContent == "EXPAND DEFAULTS") {

				for (let defaultBlock of document.getElementsByClassName('StoreBlock'))
					defaultBlock.classList.add('F');

				that.b10.textContent = "COLLAPSE DEFAULTS";

			} else {

				for (let defaultBlock of document.getElementsByClassName('StoreBlock'))
					defaultBlock.classList.remove('F');

				that.b10.textContent = "EXPAND DEFAULTS";
			}

		}.bind(this));

		this.b10.style.width = '162px';

		this.searchInput.addEventListener("keypress", this.search.bind(this));

		//  this.btnResetAll.bind(this)
	}

	createBtn(container, name, func) {
		const btn = document.createElement("div");
		btn.className = "NavBtn";
		btn.innerText = name;
		btn.onclick = func;
		container.appendChild(btn);
		return btn;
	}

	getStoreList(slot) {

		if (this.storeContainer) {
			return;
		}

		if (slot instanceof MouseEvent) {
			slot = null;
		}

		this.nodeSlot = slot;

		// ***
		// DRAW DIALOG MESSAGE
		// ***
		const d = document.createElement("div");
		this.storeContainer = d;
		d.className = "StoresBackground";

		// Main Container
		const div = document.createElement("div");
		div.className = "StoresContainer";
		d.appendChild(div);

		// Switch beetwen Store and Defaults
		const div_Switch = document.createElement("div");
		div_Switch.className = "SwitchContainer";
		div.appendChild(div_Switch);

		const btn_s = document.createElement("div");
		btn_s.className = "BtnSwitch";
		btn_s.innerText = "Store";
		btn_s.style.fontWeight = "bold";
		div_Switch.appendChild(btn_s);

		let ddd = null;
		if (this.nodeSlot) {

			const btn_d = document.createElement("div");
			btn_d.className = "BtnSwitch";
			btn_d.innerText = "Default";
			div_Switch.appendChild(btn_d);

			// Set Defaults
			const div_d = document.createElement("div");
			div_d.className = "DefaultsContainer";
			div_d.innerText = "\nEnter Default value:";
			div_d.style.display = "none";
			div.appendChild(div_d);

			const input_def = document.createElement("input");
			input_def.className = "DefaultsInput";
			div_d.appendChild(document.createElement("br"));
			div_d.appendChild(input_def);
			if (this.nodeSlot.defaults) {
				input_def.value = this.nodeSlot.defaults.value;
			}

			// SWITCH CLICKS EVENTS
			btn_s.onclick = this.changeToStore;
			btn_d.onclick = this.changeToDefaults;
			ddd = btn_d;

		}

		// List STORES
		const div2 = document.createElement("div");
		div2.className = "StoresContainer2";
		div.appendChild(div2);

		// ***
		// PULL STORES:
		// ***
		for (const l in APP.Logic.stores) {

			const v = APP.Logic.stores[l];

			const lineDiv = document.createElement("div");
			lineDiv.className = "StoreItem";
			lineDiv.innerText = l;
			div2.appendChild(lineDiv);

			// SELECT STORE CLICK
			lineDiv.onclick = this.selectStoreEvent.bind(this)

			// REMOVE STORE
			if (!this.nodeSlot) {
				const removeStore = document.createElement("div");
				removeStore.className = "StoreRemove";
				removeStore.innerText = "x";
				removeStore.onclick = this.removeStoreEvent.bind(this);
				lineDiv.appendChild(removeStore);
			}

		}

		// ADD NEW STORE
		if (!this.nodeSlot) {
			const lineDiv = document.createElement("div");
			lineDiv.className = "StoreItem";
			lineDiv.innerText = "Add new store:";
			div2.appendChild(lineDiv);

			const input = document.createElement("input");
			input.className = "InputNewStore";
			lineDiv.appendChild(input);

			const addStore = document.createElement("div");
			lineDiv.appendChild(addStore);
			addStore.className = "StoreRemove";
			addStore.innerText = "+";
			addStore.onclick = this.createStoreEvent.bind(this);
		}

		if (this.nodeSlot) {
			const btn_ok = document.createElement("div");
			btn_ok.className = "BtnChooseStore1";
			btn_ok.innerText = "Ok"
			div.appendChild(btn_ok);
			btn_ok.onclick = this.acceptStoreDefaults.bind(this);

			if (this.nodeSlot.defaults) {
				this.changeToDefaults({ "target": ddd });
			}

		}

		const btn_c = document.createElement("div");
		btn_c.className = "BtnChooseStore2";
		btn_c.innerText = "Cancel"
		div.appendChild(btn_c);
		btn_c.onclick = this.cancelStoreDefaults.bind(this);

		document.body.appendChild(d);

	}

	selectStoreEvent(event) {
		if (this.storeSelected) {
			this.storeSelected.classList.remove("Selected");
		}
		event.target.classList.add("Selected");
		this.storeSelected = event.target;
	}

	createStoreEvent(event) {
		const text = event.target.parentElement.children[0].value;
		if (!text.length) {
			console.error("Name is empty!");
			return;
		}

		if (APP.Logic.stores.indexOf(text) !== -1) {
			console.error("Name already exist!");
			return;
		}

		const lineDiv = document.createElement("div");
		lineDiv.className = "StoreItem";
		lineDiv.innerText = text;
		lineDiv.onclick = this.selectStoreEvent.bind(this)

		if (!this.nodeSlot) {
			const removeStore = document.createElement("div");
			lineDiv.appendChild(removeStore);
			removeStore.className = "StoreRemove";
			removeStore.innerText = "x";
			removeStore.onclick = this.removeStoreEvent.bind(this);
		}

		APP.Logic.stores.push(text);

		event.target.parentElement.parentElement.insertBefore(lineDiv, event.target.parentElement);
		event.target.parentElement.children[0].value = "";

	}

	removeStoreEvent(event) {
		console.log("REMOVE STORE", e.target.parentElement.textContent);
		// TODO REMOVE
	}

	acceptStoreDefaults(event) {

		if (this.nodeSlot && this.nodeSlot instanceof Slot) {
			if (this.storeSelected) {
				this.nodeSlot.appendStoreToSlot(this.storeSelected.innerText);
			} else {
				const def = this.storeContainer.children[0].children[1].children[2].value;
				APP.Logic.nodes[this.nodeSlot.nodeID].appendDefaults(this.nodeSlot, {
					"index": this.nodeSlot.index,
					"value": def
				});
			}
		}

		console.log("OK");

		this.storeContainer.remove();
		this.storeContainer = null;
		this.storeSelected = null;
		this.nodeSlot = null;
	}

	cancelStoreDefaults(event) {
		this.storeContainer.remove();
		this.storeContainer = null;
		this.storeSelected = null;
		this.nodeSlot = null;
	}

	changeToStore(event) {
		const def = event.target.parentElement.parentElement.children[1];
		const str = event.target.parentElement.parentElement.children[2];
		def.style.display = "none";
		str.style.display = "";
		event.target.style.fontWeight = "bold";
		event.target.parentElement.children[1].style.fontWeight = "";
	}

	changeToDefaults(event) {
		const def = event.target.parentElement.parentElement.children[1];
		const str = event.target.parentElement.parentElement.children[2];
		def.style.display = "";
		str.style.display = "none";
		event.target.style.fontWeight = "bold";
		event.target.parentElement.children[0].style.fontWeight = "";

		if (this.storeSelected) {
			this.storeSelected.classList.remove("Selected");
			this.storeSelected = null;
		}

	}

	btnResetAll() {
		resetAll();
	}


	search(e) {
		if (!e) e = window.event;
		let keyCode = e.keyCode || e.which;
		if (keyCode != '13') {
			return;
		}

		let node = null;
		let value = e.target.value;


		if (!isNaN(value)) {
			// search by ID
			node = APP.Logic.nodes[parseInt(value)];
		} else {
			// search by str
			for (let nodeID in APP.Logic.nodes) {
				let n = APP.Logic.nodes[nodeID];
				if (!n) {
					continue;
				}
				if (n.type.indexOf(value) !== -1) {
					node = n;
					break;
				}
			}
		}

		if (!node) {
			return;
		}

		this.camZ = 2;
		let nx = node.htmlElement.offsetLeft;
		let ny = node.htmlElement.offsetTop;
		let nw = node.htmlElement.offsetWidth / 2;
		let nh = node.htmlElement.offsetHeight / 2;

		let vec = this.translatePosToCenter(nx + nw, ny + nh);
		this.camX = vec[0];
		this.camY = vec[1];

		this.updateScene();
	}


	translatePosTo(x, y) {
		let w = window.innerWidth / 2;
		let h = window.innerHeight / 2;
		let _x = (x - w) / this.camZ + w + this.camX;
		let _y = (y - h) / this.camZ + h + this.camY;
		return [_x, _y];
	}

	translatePosFrom(x, y) {
		let w = window.innerWidth / 2;
		let h = window.innerHeight / 2;
		let camX = x - w - (- w / this.camZ);
		let camY = y - h - (- h / this.camZ);
		return [camX, camY];
	}

	translatePosToCenter(x, y) {
		let w = window.innerWidth / 2;
		let h = window.innerHeight / 2;
		let camX = x - w - (- w / this.camZ) - (w / this.camZ);
		let camY = y - h - (- h / this.camZ) - (h / this.camZ);
		return [camX, camY];
	}

	//˂˃˄˅◄►▲▼
	createStoreBlock() {

		this.staticStoreView = document.createElement("div");
		this.staticStoreView.id = "StaticStoreView";
		document.body.appendChild(this.staticStoreView);

		let buttonControl = document.createElement("div");
		buttonControl.className = "BtnControl";
		buttonControl.textContent = "HIDE";
		buttonControl.addEventListener("click", this.hideViewControls.bind(this));
		this.staticStoreView.appendChild(buttonControl);

		for (let store in APP.Logic.stores) {

			let storeHtml = document.createElement("div");
			this.staticStoreView.appendChild(storeHtml);
			storeHtml.textContent = store;

		}


	}

	createLibrares() {

		this.libraresView = document.createElement("div");
		// this.libraresView.style.display = 'none';//redesign
		this.libraresView.id = "LibraresView";
		document.body.appendChild(this.libraresView);

		let buttonControl = document.createElement("div");
		buttonControl.className = "BtnControl";
		buttonControl.textContent = "HIDE";
		buttonControl.addEventListener("click", this.hideViewControls.bind(this));
		this.libraresView.appendChild(buttonControl);

		APP.Logic.LibData = {};
		let counter = 0;

		for (let libID in library.nodes) {
			let lib = library.nodes[libID];

			let id = "lib_" + counter++;
			let storeHtml = document.createElement("div");
			if (lib.name) {
				storeHtml.textContent = lib.name;
			} else {
				storeHtml.textContent = lib.type;
			}
			storeHtml.id = id;
			storeHtml.draggable = true;
			storeHtml.addEventListener("dragstart", this.dragStart.bind(this));
			storeHtml.addEventListener("dragend", this.dragEnd.bind(this));
			this.libraresView.appendChild(storeHtml);

			APP.Logic.LibData[id] = lib;

		}


	}


	hideViewControls(event) {

		let btn = event.target;
		let text = btn.textContent;
		let parent = event.target.parentElement;

		if (text == "HIDE") {
			btn.textContent = "SHOW";
			parent.style.width = "50px";
			parent.style.height = "20px";
			parent.style.overflow = "hidden";
			btn.style.lineHeight = "normal";
			btn.style.textIndent = "0px";

		} else {
			btn.textContent = "HIDE";
			parent.style.width = "";
			parent.style.height = "";
			parent.style.overflow = "";
			btn.style.lineHeight = "";
			btn.style.textIndent = "";
		}

	}

	dragStart(event) {
		// console.log("start", event);
		APP.Logic.DROP_OBJ = event.target.id;
	}

	dragEnd(event) {
		// console.log("end", event);
		APP.Logic.DROP_OBJ = null;
	}

}






// function checkFocused(event) {
// 	const _target = event.target;
// 	if (!_target) {
// 		return;
// 	}

// 	// eslint-disable-next-line no-extra-parens
// 	const path = event.path || (event.composedPath && event.composedPath());
// 	for (const n of path) {
// 		if (!n) {
// 			continue;
// 		}
// 		if (!n.classList) {
// 			continue;
// 		}
// 		// Node or Connection
// 		if (n.classList.contains("node")) {
// 			const node = GlobalNodes[n.id];
// 			if (FocusedElement === node) {
// 				return;
// 			}
// 			clearFocudes();
// 			FocusedElement = node;
// 			FocusedElement.focused(true);
// 			return;
// 		} else if (n.classList.contains("connection_path")) {
// 			for (const connection of GlobalConnections) {
// 				if (!connection) {
// 					continue;
// 				}
// 				if (connection.htmlSupportLine === event.target) {
// 					// eslint-disable-next-line max-depth
// 					if (FocusedElement === connection) {
// 						return;
// 					}
// 					clearFocudes();
// 					FocusedElement = connection;
// 					FocusedElement.focused(true);
// 					return;
// 				}

// 			}
// 			return;
// 		}
// 	}

// 	clearFocudes();

// }
/*
// eslint-disable-next-line no-unused-vars
function savePositions() {
	for (const bId in GlobalNodes) {
		if (!bId) {
			continue;
		}

		const bNode = GlobalNodes[bId];
		for (const oId in script.nodes) {
			if (!oId) {
				continue;
			}

			const oNode = script.nodes[oId];
			if ((bNode.type === oNode.type) && (bNode.id === oNode.id)) {
				oNode.x = bNode.htmlElement.offsetLeft;
				oNode.y = bNode.htmlElement.offsetTop;
				console.log(oNode, bNode);
			}
		}
	}
	console.log(JSON.stringify(script));
}



*/