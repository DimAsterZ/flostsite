

let redesignThemeNextId = 0;

class Redesigner {

	static instance = new Redesigner();

	static inactiveParameters = null;

	constructor() {

		this.footerBaseHeight = 16;

		this.footerClipOffset = 73;

		this.enabled = document.getElementById('redesignStyles') != null;

		this.currentThemeState = {};

		this.sizeInputs = {};
		this.colorInputs = {};
		this.switcherInputs = {};

		this.themeList = [];

		this.currentThemeIndex = 0;
		this.currentPaletteIndex = 0;

		this.saveThemeButton = null;

		this.paletteHtml = null;

		this.designConfigMenuHtmlContainer = null;

		this.themeConfigMenu = null;

		this.parseThemes();
	}

	parseThemes() {

		let themesIds = Object.keys(redesignThemes);

		for (let id of themesIds) {
			let intId = parseInt(id);
			if (intId > redesignThemeNextId)
				redesignThemeNextId = intId;
		}

		redesignThemeNextId++;

		this.themeList = themesIds;

		this.currentThemeIndex = 0;//themesIds.length - 1;

		let currentThemeId = themesIds[this.currentThemeIndex];

		let theme = redesignThemes[currentThemeId];

		this.currentThemeState = JSON.parse(JSON.stringify(theme.state));

		this.currentPaletteIndex = theme.paletteIndex;
	}

	getConnectionPointSize() {

		return parseInt(this.currentThemeState['--connectionPointSize']);
	}

	removeBlockIndents() {

		let indentElements = [...document.getElementsByClassName('node-b-indent1'),
		...document.getElementsByClassName('node-info')];

		for (let i = indentElements.length - 1; i >= 0; --i)
			indentElements[i].remove();

		let connections = APP.Logic.connections;

		for (let key in connections)
			connections[key].updatePositions();
	}

	fixBlockFooterClipPath() {

		let borderWidth = parseInt(this.currentThemeState['--blockBorderWidth']);
		// let borderWidth = parseInt(getComputedStyle(document.documentElement)
		// 	.getPropertyValue('--blockBorderWidth'));

		let pathContainer = document.getElementById('clipPathTemplate2');

		for (let child of pathContainer.children) {

			if (child.tagName === 'path') {

				let height = this.footerBaseHeight + borderWidth;

				let data = 'M 0 0 L 200 0';

				data += ' L 200 ' + height;

				data += ' L ' + this.footerClipOffset * (1 + borderWidth / (this.footerBaseHeight + 1));

				data += ' ' + height;

				data += ' L 0 1';

				child.setAttribute('d', data);

				return;
			}
		}
	}

	fixBlockFooter() {


		this.fixBlockFooterClipPath();
	}

	addGrid() {

		let body = document.body;

		let gridSector = document.createElement('div');

		this.designConfigMenuHtmlContainer = gridSector;

		gridSector.classList.add('gridSector');



		// let bodyChildren = body.children;

		// for (let child of bodyChildren)
		// 	gridSector.appendChild(child);

		// body.appendChild(gridSector);

	}

	switchHeaderGradient() {

		let varName = '--blockHeaderBackground';

		let background = this.currentThemeState[varName];
		// let background = getComputedStyle(document.documentElement)
		// 	.getPropertyValue(varName);

		let rootStyle = document.documentElement.style;

		let colorVarName = 'var(--blockHeaderBackgroundColor)';

		if (background.includes('gradient'))
			rootStyle.setProperty(varName, colorVarName);
		else
			rootStyle.setProperty(varName, 'var(--blockHeaderBackgroundGradient)');
	}

	switchHeaderGradientFocused() {

		let varName = '--blockHeaderBackgroundFocused';

		let background = this.currentThemeState[varName];
		// let background = getComputedStyle(document.documentElement)
		// 	.getPropertyValue(varName);

		let rootStyle = document.documentElement.style;

		let colorVarName = 'var(--blockHeaderBackgroundColorFocused)';

		if (background.includes('gradient'))
			rootStyle.setProperty(varName, colorVarName);
		else
			rootStyle.setProperty(varName, 'var(--blockHeaderBackgroundGradientFocused)');
	}

	switchFooterClip() {

		let varName = '--blockFooterClipPath';

		let clip = this.currentThemeState[varName];
		// let clip = getComputedStyle(document.documentElement)
		// 	.getPropertyValue(varName);

		let rootStyle = document.documentElement.style;

		if (clip == 'none')
			rootStyle.setProperty(varName, 'var(--blockFooterClipPathTemplate)');
		else
			rootStyle.setProperty(varName, 'none');
	}


	formatVarName(name, header) {

		name = name.replace(header,'');
		name = name.replace('BackgroundColor','Background');
		name = name.replace('FontColor','Font');

		let upper = name.toUpperCase();

		let j = 0;

		for(let i = 0; i < upper.length; ++i) {

			if(upper[i] != name[j]) {

				++j;

				continue;
			}
			
			name = name.slice(0, j) + ' ' + name.slice(j);
			
			j += 2;

		}

		if(name[0] == ' ')
			name = name.substring(1);

		return name.toLowerCase();
	}

	addConfigMenuSizeItems(menu, header) {

		let sizeVars = [
			'--blockHeaderBackgroundGradientPosX',
			'--blockHeaderBackgroundGradientPosY',
			'--blockBorderWidth',
			'--connectionPointSize',
			'--connectionPointFlowBorderWidth',
			'--connectionPointDataBorderWidth',
			'--connectionPathFlowWidth',
			'--connectionPathFlowWidthFocused',
			'--connectionPathDataWidth',
			'--connectionPathDataWidthFocused',
			'--connectionPathWidthHovered',
			'--gridCellSize',
			'--gridCellBorderSize',
			'--gridSectorSize',
			'--gridSectorBorderSize',
			'--blockBorderRadius'
		]

		let root = document.documentElement;

		for (let sizeVar of sizeVars) {

			document.documentElement.style.setProperty(sizeVar, this.currentThemeState[sizeVar]);

			if(Redesigner.inactiveParameters.has(sizeVar))
				continue;

			let row = menu.insertRow();

			var cellName = row.insertCell();
			var cellInput = row.insertCell();

			cellInput.style.padding = '0';

			cellName.style.textAlign = 'right';
			cellName.style.fontSize = 'auto';
			cellName.textContent = this.formatVarName(sizeVar, header);

			cellName.classList.add('configMenuVarName');

			let input = document.createElement('input');

			this.sizeInputs[sizeVar] = input;

			input.setAttribute('type', 'number');
			input.setAttribute('min', '0');

			input.style.width = '46px';
			input.style.marginLeft = '6px';

			input.classList.add('configMenuRow');

			input.value = parseInt(this.currentThemeState[sizeVar]);
			// input.value = parseInt(getComputedStyle(root).getPropertyValue(sizeVar));

			let that = this;

			input.addEventListener('input', function (e) {

				let value = e.target.value + 'px';

				that.currentThemeState[sizeVar] = value;

				root.style.setProperty(sizeVar, value);

				that.onThemeUpdate();
			});

			cellInput.appendChild(input);
		}
	}

	parseCSSColorStr(colorStr) {

		if (colorStr[0] == '#')
			this.parseCSSColorStr(this.hexToRGBAStr(colorStr, 1));

		let splitter = ',';

		if (!colorStr.includes(splitter))
			splitter = ' ';

		let c = colorStr.split('(')[1].split(')')[0].split(splitter);

		if (c.length < 4)
			c.push(1);

		return {
			r: parseInt(c[0]),
			g: parseInt(c[1]),
			b: parseInt(c[2]),
			a: parseFloat(c[3])
		}
	}

	parseAlphaFromRGBAStr(colorStr) {

		let splitter = ',';

		if (colorStr.includes(',')) {

			if (colorStr.split(',').length < 3)
				return 1;

		} else {

			splitter = ' ';
		}

		let start = colorStr.lastIndexOf(splitter);

		return colorStr.substring(start + 1, colorStr.length - 1);
	}

	hexToRGBAStr(hexStr, alpha) {

		let r = parseInt(hexStr.substring(1, 3), 16);
		let g = parseInt(hexStr.substring(3, 5), 16);
		let b = parseInt(hexStr.substring(5, 7), 16);

		return 'rgba(' + r + ',' + g + ',' + b + ',' + alpha + ')';
	}

	colorToRGBAStr(c) {

		return 'rgba(' + c.r + ', ' + c.g + ', ' + c.b + ', ' + c.a + ')';
	}

	RGBToHex(c) {

		let rStr = c.r.toString(16);
		let gStr = c.g.toString(16);
		let bStr = c.b.toString(16);

		return '#' + (rStr.length < 2 ? '0' : '') + rStr + (gStr.length < 2 ? '0' : '') + gStr + (bStr.length < 2 ? '0' : '') + bStr;
	}


	createPaletteListElement(container, palette, list, varName, activeColorIndex = 0) {

		if (list === null) {

			list = document.createElement('select');

			container.appendChild(list);

			list.style.width = '46px';

		} else {

			while (list.firstChild)
				list.firstChild.remove();

			list.removeEventListener('change', this.colorInputs[varName].selectListHandler);
		}

		list.style.background = palette[activeColorIndex];


		for (let color of palette) {

			let item = document.createElement('option');

			item.style.background = color;

			list.appendChild(item);
		}

		list.children[activeColorIndex].setAttribute('selected', '');

		let onChange = function (e) {

			let index = e.target.selectedIndex;

			list.style.background = palette[index];

			let color = that.parseCSSColorStr(that.currentThemeState[varName]);

			let paletteColor = that.parseCSSColorStr(palette[index]);

			let colorStr = that.colorToRGBAStr({ r: paletteColor.r, g: paletteColor.g, b: paletteColor.b, a: color.a });

			let root = document.documentElement;
			that.currentThemeState[varName] = colorStr;
			root.style.setProperty(varName, colorStr);

			that.onThemeUpdate();

		}

		this.colorInputs[varName].selectListHandler = onChange;

		let that = this;

		list.addEventListener('change', onChange);

		return list;
	}

	addConfigMenuColorItems(menu, colorVars, header) {

		let root = document.documentElement;

		for (let colorVar of colorVars) {

			
			document.documentElement.style.setProperty(colorVar, this.currentThemeState[colorVar]);

			if(Redesigner.inactiveParameters.has(colorVar))
				continue;

			var row = menu.insertRow();

			let color = this.currentThemeState[colorVar];
			// let color = getComputedStyle(root).getPropertyValue(colorVar);

			let that = this;

			var cellName = row.insertCell();
			var cellInput = row.insertCell();

			cellInput.style.padding = '0';

			cellName.style.textAlign = 'right';
			cellName.style.fontSize = 'auto';
			cellName.textContent = this.formatVarName(colorVar, header);

			cellName.classList.add('configMenuVarName');

			// let inputColor = document.createElement('input');

			// inputColor.setAttribute('type', 'color');

			// inputColor.style.width = '40px';

			// inputColor.classList.add('configMenuRow');

			// inputColor.value = this.RGBToHex(this.parseCSSColorStr(color));

			// this.currentThemeState[colorVar] = color;

			// inputColor.addEventListener('input', function (e) {

			// 	let alpha = that.parseAlphaFromRGBAStr(that.currentThemeState[colorVar]);

			// 	let colorStr = that.hexToRGBAStr(e.target.value, alpha);

			// 	that.currentThemeState[colorVar] = colorStr;

			// 	root.style.setProperty(colorVar, colorStr);
			// });

			let inputAlpha = document.createElement('input');



			inputAlpha.setAttribute('type', 'number');
			inputAlpha.setAttribute('min', '0');
			inputAlpha.setAttribute('max', '100');
			inputAlpha.setAttribute('step', '4');

			inputAlpha.style.width = '46px';
			inputAlpha.style.marginLeft = '6px';

			inputAlpha.classList.add('configMenuRow');

			inputAlpha.value = parseInt(this.parseCSSColorStr(color).a * 100);

			inputAlpha.addEventListener('input', function (e) {

				let root = document.documentElement;

				let color = that.parseCSSColorStr(that.currentThemeState[colorVar]);

				let alpha = parseInt(e.target.value) / 100;

				let colorStr = that.colorToRGBAStr({ r: color.r, g: color.g, b: color.b, a: alpha });

				that.currentThemeState[colorVar] = colorStr;

				root.style.setProperty(colorVar, colorStr);

				that.onThemeUpdate();
			});


			cellInput.appendChild(inputAlpha);

			let palette = redesignPalettes[redesignThemes[this.themeList[this.currentThemeIndex]].paletteIndex];

			this.colorInputs[colorVar] = { alpha: inputAlpha };

			this.colorInputs[colorVar].paletteList = this.createPaletteListElement(cellInput, palette, null, colorVar, palette.indexOf(color.substring(0, color.lastIndexOf(',')) + ', 1)'));
			// cellInput.appendChild(inputColor);
			//tr[0].textContent = ";sdfsd";

			// menu.appendChild(li);
		}
	}

	updateSizeInputs() {

		for (let sizeVar in this.sizeInputs) {

			this.sizeInputs[sizeVar].value = parseInt(this.currentThemeState[sizeVar]);

			document.documentElement.style.setProperty(sizeVar, this.currentThemeState[sizeVar]);
		}

	}

	updateColorInputs() {

		let colorInputs = this.colorInputs;

		let currentThemeState = this.currentThemeState;

		for (let colorVar in colorInputs) {

			let color = currentThemeState[colorVar];
			// let color = getComputedStyle(root).getPropertyValue(colorVar);

			let colorInput = colorInputs[colorVar];

			//colorInput.color.value = this.RGBToHex(this.parseCSSColorStr(color));

			colorInput.alpha.value = parseInt(this.parseCSSColorStr(color).a * 100);

			let palette = redesignPalettes[redesignThemes[this.themeList[this.currentThemeIndex]].paletteIndex];

			this.createPaletteListElement(colorInput.alpha.parentElement, palette, colorInput.paletteList, colorVar, palette.indexOf(color.substring(0, color.lastIndexOf(',')) + ', 1)'));

			document.documentElement.style.setProperty(colorVar, color);
		}

	}

	updateSwitchers() {

		// this.addHeaderGradientSwitcher(headerCell,
		// 	'blockHeaderGradient',
		// 	'--blockHeaderBackground',
		// 	'--blockHeaderBackgroundColor',
		// 	'--blockHeaderBackgroundGradient');

		// this.addHeaderGradientSwitcher(headerCell,
		// 	'blockHeaderGradientFocused',
		// 	'--blockHeaderBackgroundFocused',
		// 	'--blockHeaderBackgroundColorFocused',
		// 	'--blockHeaderBackgroundGradientFocused');

		// this.addFooterClipSwitcher(headerCell);

		// this.addBorderSwitcher(cell, 'connectionPointFlowBorder', '--connectionPointFlowBorderStyle');

		// this.addBorderSwitcher(cell, 'connectionPointDataBorder', '--connectionPointDataBorderStyle');

		let input = this.switcherInputs['--blockHeaderBackground'];

		let backgroundValue = this.currentThemeState['--blockHeaderBackground'];

		document.documentElement.style.setProperty('--blockHeaderBackground', backgroundValue);

		if (backgroundValue != 'var(--blockHeaderBackgroundColor)')
			input.setAttribute('checked', '');
		else
			input.removeAttribute('checked');

		input = this.switcherInputs['--blockHeaderBackgroundFocused'];

		backgroundValue = this.currentThemeState['--blockHeaderBackgroundFocused'];

		document.documentElement.style.setProperty('--blockHeaderBackgroundFocused', backgroundValue);

		if (backgroundValue != 'var(--blockHeaderBackgroundColorFocused)')
			input.setAttribute('checked', '');
		else
			input.removeAttribute('checked');

		input = this.switcherInputs['--blockFooterClipPath'];

		let blockFooterClipPath = this.currentThemeState['--blockFooterClipPath'];

		document.documentElement.style.setProperty('--blockFooterClipPath', blockFooterClipPath);

		if (blockFooterClipPath.replace(/\s/g, '') != 'none')
			input.setAttribute('checked', '');
		else
			input.removeAttribute('checked');

		input = this.switcherInputs['--connectionPointFlowBorderStyle'];

		let connectionPointFlowBorderStyle = this.currentThemeState['--connectionPointFlowBorderStyle'];

		document.documentElement.style.setProperty('--connectionPointFlowBorderStyle', connectionPointFlowBorderStyle);

		if (connectionPointFlowBorderStyle.replace(/\s/g, '') != 'none')
			input.setAttribute('checked', '');
		else
			input.removeAttribute('checked');

		input = this.switcherInputs['--connectionPointDataBorderStyle'];

		let connectionPointDataBorderStyle = this.currentThemeState['--connectionPointDataBorderStyle'];

		document.documentElement.style.setProperty('--connectionPointDataBorderStyle', connectionPointDataBorderStyle);

		if (connectionPointDataBorderStyle.replace(/\s/g, '') != 'none')
			input.setAttribute('checked', '');
		else
			input.removeAttribute('checked');
	}

	addHeaderGradientSwitcher(cell, name, blockHeaderBackgroundVar, blockHeaderBackgroundColorVar, blockHeaderBackgroundGradientVar) {


		document.documentElement.style.setProperty(blockHeaderBackgroundGradientVar, this.currentThemeState[blockHeaderBackgroundGradientVar]);
		document.documentElement.style.setProperty(blockHeaderBackgroundVar, this.currentThemeState[blockHeaderBackgroundVar]);

		let input = document.createElement('input');

		input.setAttribute('type', 'checkbox');

		input.style.width = '14px';
		input.style.verticalAlign = 'middle';
		input.style.margin = '0px';

		input.classList.add('configMenuRow');

		let backgroundValue = this.currentThemeState[blockHeaderBackgroundVar];

		if (backgroundValue != 'var(' + blockHeaderBackgroundColorVar + ')')
			input.setAttribute('checked', '');

		let that = this;

		input.addEventListener('change', function (e) {

			let value = e.target.checked ? blockHeaderBackgroundGradientVar : blockHeaderBackgroundColorVar;

			let rootStyle = document.documentElement.style;

			that.currentThemeState[blockHeaderBackgroundVar] = 'var(' + value + ')';

			rootStyle.setProperty(blockHeaderBackgroundVar, 'var(' + value + ')');

			that.onThemeUpdate();
		});

		this.switcherInputs[blockHeaderBackgroundVar] = input;

		cell.appendChild(input);
	}

	addFooterClipSwitcher(menu) {

		let row = menu.insertRow();

		let blockFooterClipPathVar = '--blockFooterClipPath';
		let blockFooterClipPathTemplateVar = '--blockFooterClipPathTemplate';

		document.documentElement.style.setProperty(blockFooterClipPathVar, this.currentThemeState[blockFooterClipPathVar]);
		document.documentElement.style.setProperty(blockFooterClipPathTemplateVar, this.currentThemeState[blockFooterClipPathTemplateVar]);

		let name = document.createElement('div');

		name.textContent = 'footer clip ';

		name.style.display = 'inline';
		name.style.verticalAlign = 'middle';

		let input = document.createElement('input');

		input.setAttribute('type', 'checkbox');

		input.style.width = '14px';
		input.style.verticalAlign = 'middle';
		input.style.margin = '0px';

		input.classList.add('configMenuRow');

		let clip = this.currentThemeState[blockFooterClipPathVar].replace(/\s/g, '');

		if (clip != 'none')
			input.setAttribute('checked', '');

		let that = this;

		input.addEventListener('change', function (e) {

			let rootStyle = document.documentElement.style;

			let value = e.target.checked ? 'var(' + blockFooterClipPathTemplateVar + ')' : 'none';

			rootStyle.setProperty(blockFooterClipPathVar, value);

			that.currentThemeState[blockFooterClipPathVar] = value;

			that.onThemeUpdate();
		});

		row.appendChild(name);
		row.appendChild(input);

		this.switcherInputs[blockFooterClipPathVar] = input;
	}

	addBorderSwitcher(cell, name, borderVar) {

		document.documentElement.style.setProperty(borderVar, this.currentThemeState[borderVar]);

		let input = document.createElement('input');

		input.setAttribute('type', 'checkbox');

		input.style.width = '14px';

		input.style.verticalAlign = 'middle';

		input.classList.add('configMenuRow');

		let clip = this.currentThemeState[borderVar].replace(/\s/g, '');

		if (clip != 'none')
			input.setAttribute('checked', '');

		let that = this;

		input.addEventListener('change', function (e) {

			let rootStyle = document.documentElement.style;

			rootStyle.setProperty(borderVar, e.target.checked ? 'solid' : 'none');

			that.currentThemeState[borderVar] = e.target.checked ? 'solid' : 'none';

			that.onThemeUpdate();
		});

		// cell.appendChild(input);

		this.switcherInputs[borderVar] = input;
	}

	addSwitchers(menu) {

		let row = menu.insertRow();

		// let cell = row.insertCell();

		row.textContent = 'header gradient ';
		row.style.fontSize = '14px';

		// cell.style.fontSize = '14px';

		this.addHeaderGradientSwitcher(row,
			'blockHeaderGradient',
			'--blockHeaderBackground',
			'--blockHeaderBackgroundColor',
			'--blockHeaderBackgroundGradient');

		// let focused = document.createElement('div');

		// focused.textContent = 'focused';
		// focused.style.display = 'inline';

		// focused.style.verticalAlign = 'middle';

		// headerCell.appendChild(focused);

		// this.addHeaderGradientSwitcher(headerCell,
		// 	'blockHeaderGradientFocused',
		// 	'--blockHeaderBackgroundFocused',
		// 	'--blockHeaderBackgroundColorFocused',
		// 	'--blockHeaderBackgroundGradientFocused');

		this.addFooterClipSwitcher(menu);

		// cell.textContent = 'connectionPointBorder flow';

		this.addBorderSwitcher(menu, 'connectionPointFlowBorder', '--connectionPointFlowBorderStyle');

		// let data = document.createElement('div');

		// data.textContent = 'data';
		// data.style.display = 'inline';

		// data.style.verticalAlign = 'middle';

		// cell.appendChild(data);

		this.addBorderSwitcher(menu, 'connectionPointDataBorder', '--connectionPointDataBorderStyle');
	}

	setCurrentTheme(index) {

		this.currentThemeIndex = index;

		this.currentThemeState = redesignThemes[this.themeList[index]].state;

		this.updateSizeInputs();

		this.updateColorInputs();

		this.updateSwitchers();

		this.currentPaletteIndex = redesignThemes[this.themeList[index]].paletteIndex;

		this.paletteIndexHtml.textContent = this.currentPaletteIndex;

		this.paletteHtml.style.background = this.makePaletteBackground(redesignPalettes[this.currentPaletteIndex]);
	}

	makePaletteBackground(palette) {

		let colorCount = palette.length;

		let step = 100 / colorCount;

		let res = 'linear-gradient(90deg, ';

		res += palette[0] + ' ' + step + '%';

		for (let i = 1; i < colorCount; ++i)
			res += ',' + palette[i] + ' ' + (step * i) + '% ' + (step * (i + 1)) + '%';

		res += ')';

		return res;
	}

	addPaletteSelector(cell) {

		let that = this;


		let paletteIndexHtml = document.createElement('div');
		paletteIndexHtml.style.color = "black"
		paletteIndexHtml.style.textShadow = "-1px 0 rgb(255, 255, 255), 0 1px rgb(255, 255, 255), 1px 0 rgb(255, 255, 255), 0 -1px rgb(255, 255, 255)";
		paletteIndexHtml.textContent = this.currentPaletteIndex;
		paletteIndexHtml.style.fontSize = '25px';
		paletteIndexHtml.style.display = 'block';
		paletteIndexHtml.style.textAlign = 'center';
		this.paletteIndexHtml = paletteIndexHtml;

		let paletteHtmlWidth = '90px';

		let paletteHtml = document.createElement('div');

		paletteHtml.style.width = paletteHtmlWidth;

		paletteHtml.style.border = 'black solid 1px';

		paletteHtml.style.height = '49px';

		paletteHtml.style.background = this.makePaletteBackground(redesignPalettes[redesignThemes[this.themeList[this.currentThemeIndex]].paletteIndex]);

		this.paletteHtml = paletteHtml;

		let prevPalette = document.createElement('button');

		prevPalette.classList.add('paletteSelector');

		prevPalette.textContent = '<';

		prevPalette.addEventListener('click', function (e) {

			let index = that.currentPaletteIndex;

			if (index == 0)
				return;

			that.currentPaletteIndex--;

			paletteIndexHtml.textContent = that.currentPaletteIndex;

			paletteHtml.style.background = that.makePaletteBackground(redesignPalettes[that.currentPaletteIndex]);

			let colorInputs = that.colorInputs;

			let currentThemeState = that.currentThemeState;

			for (let colorVar in colorInputs) {

				let palette = redesignPalettes[that.currentPaletteIndex];

				let paletteColor = that.parseCSSColorStr(palette[0]);

				let color = that.parseCSSColorStr(currentThemeState[colorVar]);

				let colorStr = that.colorToRGBAStr({ r: paletteColor.r, g: paletteColor.g, b: paletteColor.b, a: color.a });

				let colorInput = colorInputs[colorVar];

				that.createPaletteListElement(colorInput.alpha.parentElement, palette, colorInput.paletteList, colorVar);

				currentThemeState[colorVar] = colorStr;

				document.documentElement.style.setProperty(colorVar, colorStr);
			}


			that.onThemeUpdate();

		});

		let nextPalette = document.createElement('button');

		nextPalette.classList.add('paletteSelector');

		nextPalette.textContent = '>';

		nextPalette.addEventListener('click', function (e) {

			let index = that.currentPaletteIndex;

			if (index == redesignPalettes.length - 1)
				return;

			that.currentPaletteIndex++;

			paletteIndexHtml.textContent = that.currentPaletteIndex;

			paletteHtml.style.background = that.makePaletteBackground(redesignPalettes[that.currentPaletteIndex]);

			let colorInputs = that.colorInputs;

			let currentThemeState = that.currentThemeState;

			for (let colorVar in colorInputs) {

				let palette = redesignPalettes[that.currentPaletteIndex];

				let paletteColor = that.parseCSSColorStr(palette[0]);

				let color = that.parseCSSColorStr(currentThemeState[colorVar]);

				let colorStr = that.colorToRGBAStr({ r: paletteColor.r, g: paletteColor.g, b: paletteColor.b, a: color.a });

				let colorInput = colorInputs[colorVar];

				that.createPaletteListElement(colorInput.alpha.parentElement, palette, colorInput.paletteList, colorVar);

				currentThemeState[colorVar] = colorStr;

				document.documentElement.style.setProperty(colorVar, colorStr);
			}

			that.onThemeUpdate();

		});


		// next.addEventListener('click', function (e) {

		// 	let index = that.currentPalette;

		// 	if (index == that.palettes.length - 1)
		// 		return;

		// 	that.setCurrentPalette(index + 1);

		// 	currentPalette.textContent = 'palette:' + that.currentPalette;
		// });

		paletteHtml.appendChild(prevPalette);
		paletteHtml.appendChild(nextPalette);
		paletteHtml.appendChild(paletteIndexHtml);

		cell.appendChild(paletteHtml);
	}

	onThemeUpdate() {
		// this.saveThemeButton.removeAttribute('disabled');
	}

	addThemeSelector(mainCell, cell) {

		mainCell = this.designConfigMenuHtmlContainer;//document.body;//document.getElementsByClassName('gridSector')[0];

		let navWidth = '92px';

		let prev = document.createElement('button');

		prev.style.display = 'block';

		prev.style.zIndex = '11';
		prev.style.position = 'fixed';
		prev.style.left = '500px';

		let currentTheme = document.createElement('div');

		currentTheme.style.display = 'inline';

		currentTheme.style.display = 'block';
		currentTheme.style.zIndex = '1';
		currentTheme.style.position = 'fixed';
		currentTheme.style.left = '592px';
		currentTheme.style.background = 'white';

		currentTheme.style.fontSize = '16px';

		currentTheme.textContent = 'theme:' + this.themeList[this.currentThemeIndex];

		prev.innerText = "prev";

		prev.style.width = navWidth;

		let that = this;

		prev.addEventListener('click', function (e) {

			let index = that.currentThemeIndex;

			if (index == 0)
				return;

			that.setCurrentTheme(index - 1);

			currentTheme.textContent = 'theme:' + that.themeList[index - 1];
		});

		let next = document.createElement('button');

		next.style.display = 'block';
		next.style.zIndex = '11';
		next.style.position = 'fixed';
		next.style.left = '653px';

		next.innerText = "next";

		next.style.width = navWidth;

		next.addEventListener('click', function (e) {

			let index = that.currentThemeIndex;

			if (index == that.themeList.length - 1)
				return;

			that.setCurrentTheme(index + 1);

			currentTheme.textContent = 'theme:' + that.themeList[index + 1];
		});

		let addTheme = document.createElement('button');

		addTheme.classList.add('themeUpdate');

		addTheme.style.display = 'inline';

		addTheme.style.fontFamily = 'Material Icons';

		addTheme.textContent = '+';

		addTheme.addEventListener('click', function (e) {

			let id = redesignThemeNextId++;

			redesignThemes[id] = {
				paletteIndex: that.currentPaletteIndex,
				state: JSON.parse(JSON.stringify(that.currentThemeState))
			};

			that.themeList.push(id);

			that.currentThemeIndex = that.themeList.length - 1;

			currentTheme.textContent = 'theme:' + id;
		});

		let saveTheme = document.createElement('button');

		saveTheme.classList.add('themeUpdate');

		saveTheme.style.display = 'inline';

		saveTheme.style.fontFamily = 'Material Icons';

		saveTheme.textContent = 'save';

		this.saveThemeButton = saveTheme;

		saveTheme.setAttribute('disabled', '');

		saveTheme.addEventListener('click', function (e) {

			redesignThemes[that.themeList[that.currentThemeIndex]].state = JSON.parse(JSON.stringify(that.currentThemeState));

			redesignThemes[that.themeList[that.currentThemeIndex]].paletteIndex = that.currentPaletteIndex;

			that.saveThemeButton.setAttribute('disabled', '');

			function download(content, fileName, contentType) {
				var a = document.createElement("a");

				content = 'let redesignThemes = ' + content;

				var file = new Blob([content], { type: contentType });
				a.href = URL.createObjectURL(file);
				a.download = fileName;
				a.click();
			}
			download(JSON.stringify(redesignThemes, null, 2), 'RedesignThemes.js', 'text/plain');
		});

		mainCell.appendChild(prev);
		mainCell.appendChild(currentTheme);
		mainCell.appendChild(next);
		// cell.appendChild(saveTheme);
		// cell.appendChild(addTheme);
	}

	addEditThemeMenu(menu) {

		let container = this.designConfigMenuHtmlContainer;//this.designConfigMenuHtmlContainer;//document.getElementsByClassName('gridSector')[0];

		let toggleEditThemeMenu = document.createElement('button');

		toggleEditThemeMenu.style.display = 'block';
		toggleEditThemeMenu.style.zIndex = '11';
		toggleEditThemeMenu.style.position = 'fixed';
		toggleEditThemeMenu.style.left = '245px';


		toggleEditThemeMenu.style.fontFamily = 'Material Icons';

		toggleEditThemeMenu.innerText = (menu.style.display == 'block' || menu.style.display == '') ? "preview" : "edit";

		toggleEditThemeMenu.addEventListener('click', function (e) {


			if (toggleEditThemeMenu.innerText == "edit") {

				toggleEditThemeMenu.innerText = "preview";

				menu.style.display = 'block';

			} else {

				toggleEditThemeMenu.innerText = "edit";

				menu.style.display = 'none';
			}

		});

		container.appendChild(toggleEditThemeMenu);
	}

	addConfigMenu() {

		let uiMouseWheelListener = APP.UI.mouseWheel.bind(APP.UI);

		let dummyMouseWheelListener = function () { };

		document.addEventListener("mousewheel", uiMouseWheelListener);

		let menu = document.createElement('table');

		this.themeConfigMenu = menu;
		// menu.style.left = "800px";
		menu.style.top = "21px";
		menu.style.display = 'none';
		menu.style.borderStyle = 'solid';
		menu.style.borderColor = '#e4ceadb8';

		

		menu.setAttribute('cols', 2);

		menu.addEventListener('mouseenter', function () {
			document.removeEventListener("mousewheel", uiMouseWheelListener);
			document.addEventListener("mousewheel", dummyMouseWheelListener);
		})
		menu.addEventListener('mouseleave', function () { document.addEventListener("mousewheel", uiMouseWheelListener); })

		let style = menu.style;

		style.background = 'white';
		style.position = 'absolute';
		style.zIndex = '10';
		// style.width = '200px';
		// style.height = '980px';

		let nodeSectionRow = menu.insertRow();

		nodeSectionRow.textContent = "Node";
		nodeSectionRow.style.fontSize = '16px';
		// var themeSelectorCell = headerRow.insertCell();

		// themeSelectorCell.setAttribute('rowspan', '2');

		this.addSwitchers(menu);

		// this.addPaletteSelector(themeSelectorCell);

		// this.addThemeSelector(themeSelectorCell, cell);

		this.addConfigMenuSizeItems(menu, "--block");

		let colorVars = [
			'--blockHeaderBackgroundColor',
			'--blockHeaderFontColor',
			'--blockHeaderBackgroundGradientColorBegin',
			'--blockHeaderBackgroundGradientColorEnd',
			'--blockHeaderBackgroundColorFocused',
			'--blockHeaderBackgroundGradientFocusedColorBegin',
			'--blockHeaderBackgroundGradientFocusedColorEnd',
			'--blockBodyBackground',
			'--blockBodyFontColor',
			'--blockFooterBackground',
			'--blockFooterBackgroundFocused',
			'--blockFooterFontColor',
			'--blockBorderColor'
		]

		this.addConfigMenuColorItems(menu, colorVars, '--block');

		let splitRow = menu.insertRow();
		splitRow.style.height = '16px';
		let connectionSectionRow = menu.insertRow();
		connectionSectionRow.textContent = "Connection";
		connectionSectionRow.style.fontSize = '16px';

		colorVars = [
			
			'--connectionPointFlowBackground',
			'--connectionPointFlowBorderColor',
			'--connectionPointDataBackground',
			'--connectionPointDataBorderColor',
			'--connectionPathFlowColor',
			'--connectionPathFlowColorFocused',
			'--connectionPathDataColor',
			'--connectionPathDataColorFocused',
			'--connectionPathColorHovered'
		]

		this.addConfigMenuColorItems(menu, colorVars, '--connection');

		splitRow = menu.insertRow();
		splitRow.style.height = '16px';
		let gridSectionRow = menu.insertRow();
		gridSectionRow.textContent = "Grid";
		gridSectionRow.style.fontSize = '16px';

		colorVars = [
			
			'--gridCellColor',
			'--gridCellBorderColor',
			'--gridSectorBorderColor'
		]

		this.addConfigMenuColorItems(menu, colorVars, '--grid');
		
		
		splitRow = menu.insertRow();
		splitRow.style.height = '16px';

		colorVars = [
			
			'--evenViewAreaBackground',
			'--oddViewAreaBackground',
			'--handlerSectorBackground',
			'--storeBlockFontColor',
			'--storeBlockBackground',
			// '--storeBlockHoverToggleColor',
			// '--storeBlockToggleColor',
			// '--storeBlockToggleBackground',
			// '--storeBlockHoverToggleBackground'
			'--storeBlockValueTriggerColor',
			'--defaultBlockValueTriggerColor'
			// '--gridSectorColor'
		]
		this.addConfigMenuColorItems(menu, colorVars, '--');
		

		this.addEditThemeMenu(menu);

		document.body.appendChild(menu);


	}

	redesign() {

		if (!this.enabled)
			return;

		this.fixBlockFooter();
		this.addGrid();

		this.addConfigMenu();
		this.removeBlockIndents();

		// document.getElementById('StaticStoreView').remove();
		//document.getElementById('LibraresView').remove();


	}



};

Redesigner.inactiveParameters = new Set([

	'--blockHeaderBackgroundGradientPosX',
	'--blockHeaderBackgroundGradientPosY',
	'--blockBorderWidth',
	'--connectionPointSize',
	'--connectionPointFlowBorderWidth',
	'--connectionPointDataBorderWidth',
	'--connectionPathFlowWidth',
	'--connectionPathFlowWidthFocused',
	'--connectionPathDataWidth',
	'--connectionPathDataWidthFocused',
	'--connectionPathWidthHovered',
	'--gridCellSize',
	'--gridCellBorderSize',
	'--gridSectorSize',
	'--gridSectorBorderSize',


	// '--blockHeaderBackgroundColor',
	// '--blockHeaderFontColor',
	'--blockHeaderBackgroundGradientColorBegin',
	'--blockHeaderBackgroundGradientColorEnd',
	'--blockHeaderBackgroundColorFocused',
	'--blockHeaderBackgroundGradientFocusedColorBegin',
	'--blockHeaderBackgroundGradientFocusedColorEnd',
	// '--blockBodyBackground',
	// '--blockBodyFontColor',
	// '--blockFooterBackground',
	'--blockFooterBackgroundFocused',
	// '--blockFooterFontColor',
	// '--blockBorderColor',
	// '--connectionPointFlowBackground',
	// '--connectionPointFlowBorderColor',
	// '--connectionPointDataBackground',
	// '--connectionPointDataBorderColor',
	// '--connectionPathFlowColor',
	'--connectionPathFlowColorFocused',
	// '--connectionPathDataColor',
	'--connectionPathDataColorFocused',
	'--connectionPathColorHovered'
	// '--gridCellColor',
	// '--gridCellBorderColor',
	// '--gridSectorBorderColor',
	// '--evenViewAreaBackground',
	// '--oddViewAreaBackground',
	// '--handlerSectorBackground',
	// '--storeBlockFontColor',
	// '--storeBlockBackground',
	// '--storeBlockValueTriggerColor',
	// '--defaultBlockValueTriggerColor'

]);


let printThemes = function () {


	console.log(JSON.stringify(redesignThemes));
}
